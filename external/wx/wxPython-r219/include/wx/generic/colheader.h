///////////////////////////////////////////////////////////////////////////////
// Name:        include/wx/generic/colheader.h
// Purpose:     data definitions for a 2-platform (Mac,MSW) + generic native-appearance column header
// Author:      David Surovell
// Modified by:
// Created:     01.01.2005
// RCS-ID:
// Copyright:
// License:
///////////////////////////////////////////////////////////////////////////////

#if !defined(_WX_GENERIC_COLUMNHEADER_H)
#define _WX_GENERIC_COLUMNHEADER_H

#include "wx/control.h"         // wxColumnHeader base class
#include "wx/dcclient.h"
#include "wx/font.h"
#include "wx/colour.h"
#include "wx/bitmap.h"

// forward decls
class wxColumnHeaderItem;

// ----------------------------------------------------------------------------
// private data definitions
// ----------------------------------------------------------------------------

class WXDLLIMPEXP_ADV wxColumnHeader : public wxControl
{
friend class wxColumnHeaderItem;

public:
    // construction
    wxColumnHeader(
        wxWindow        *parent,
        wxWindowID      id = wxID_ANY,
        const wxPoint   &pos = wxDefaultPosition,
        const wxSize    &size = wxDefaultSize,
        long            style = 0,
        const wxString  &name = wxColumnHeaderNameStr );

    wxColumnHeader();

    virtual ~wxColumnHeader();

    bool Create(
        wxWindow        *parent,
        wxWindowID      id = wxID_ANY,
        const wxPoint   &pos = wxDefaultPosition,
        const wxSize    &size = wxDefaultSize,
        long            style = 0,
        const wxString  &name = wxColumnHeaderNameStr );


    // override some base class virtuals
    virtual void DoMoveWindow( int x, int y, int width,int height );
    
    virtual bool Enable( bool bEnable = true );

    virtual void DoSetSize( int x, int y, int width, int height, int sizeFlags );
    virtual wxSize DoGetBestSize( void ) const;
    virtual wxSize DoGetMinSize( void ) const;

    // size calcluations
    wxSize CalculateDefaultSize( void ) const;

    wxSize CalculateDefaultItemSize( const wxSize& maxSize );
    
    wxSize GetDefaultItemSize( void ) const;
    void SetDefaultItemSize( const wxSize& targetSize );

    wxSize GetTotalUIExtent( long itemCount = (-1),
                             bool bStartAtBase = false ) const;

    bool ResizeToFit( long itemCount = (-1) );
    bool RescaleToFit( long newWidth );
    bool ResizeDivision( long itemIndex,
                         long originX );

    wxColour GetSelectionColour() const;
    void SetSelectionColour( const wxColour &targetColour );

    bool GetAttribute( wxColumnHeaderAttribute flagEnum ) const;
    bool SetAttribute( wxColumnHeaderAttribute flagEnum,
                       bool                    bFlagValue );

    // returns a non-negative value for a column header item
    // or wxCOLUMNHEADER_HITTEST_NOWHERE for no item
    wxColumnHeaderHitTestResult HitTest( const wxPoint &locationPt );
    long GetSelectedItem( void ) const;
    void SetSelectedItem( long itemIndex );
    long GetBaseViewItem( void ) const;
    void SetBaseViewItem( long itemIndex );

    long GetItemCount( void ) const;
    void SetItemCount( long itemCount );

    void DeleteItems( long itemIndex,
                      long itemCount );
    void DeleteItem(  long itemIndex );
    
    void AppendItem( const wxString  &textBuffer,
                     wxAlignment     align,
                     long            extentX = (-1),
                     bool            bSelected = false,
                     bool            bSortEnabled = false,
                     bool            bSortAscending = false );
    void AddEmptyItems( long beforeIndex,
                        long itemCount );
    void AddItem( long           beforeIndex,
                  const wxString &textBuffer,
                  wxAlignment    align,
                  long           width = (-1),
                  bool           bSelected = false,
                  bool           bSortEnabled = false,
                  bool           bSortAscending = false );

    bool GetItemVisibility( long itemIndex ) const;
    void SetItemVisibility( long itemIndex,
                            bool bVisible );

    wxBitmap GetLabelBitmap( long itemIndex ) const ;
    void SetLabelBitmap( long           itemIndex,
                         const wxBitmap &imageRef );

    wxString GetLabelText( long itemIndex ) const;
    void SetLabelText( long           itemIndex,
                       const wxString &textBuffer );

    wxAlignment GetLabelAlignment( long itemIndex ) const;
    void SetLabelAlignment( long         itemIndex,
                            wxAlignment  align );


    wxPoint GetItemPosition( long itemIndex ) const;
    void SetItemPosition( long itemIndex, const wxPoint &targetPos );
    
    wxSize GetItemSize( long itemIndex ) const;
    void SetItemSize(long itemIndex, const wxSize &targetSize );

    bool GetItemAttribute( long                        itemIndex,
                           wxColumnHeaderItemAttribute flagEnum ) const;
    bool SetItemAttribute( long                        itemIndex,
                           wxColumnHeaderItemAttribute flagEnum,
                           bool                        bFlagValue );

    long GetItemLabelWidth(long itemIndex) const;
    
    
    // implementation only from now on
    // -------------------------------

    virtual wxVisualAttributes GetDefaultAttributes( void ) const
        { return GetClassDefaultAttributes( GetWindowVariant() ); }

    static wxVisualAttributes GetClassDefaultAttributes(
        wxWindowVariant variant = wxWINDOW_VARIANT_NORMAL );

    static long GetFixedHeight( void );

    static void GetDefaultLabelValue(
        bool                isVertical,
        long                itemIndex,
        wxString            &value );

protected:
    void AddItemList( const wxColumnHeaderItem    *itemList,
                      long                        itemCount,
                      long                        beforeIndex );

    void OnClick_SelectOrToggleSort( long itemIndex,
                                     bool bToggleSortDirection );

    bool GetItemData( long               itemIndex,
                      wxColumnHeaderItem *info ) const;
    bool SetItemData( long                     itemIndex,
                      const wxColumnHeaderItem *info );
    bool ItemInView(  long itemIndex ) const;
    bool GetItemBounds( long   itemIndex,
                        wxRect *boundsR ) const;
    wxColumnHeaderItem * GetItemRef( long itemIndex ) const;
    void RefreshItem( long  itemIndex,
                      bool  bForceRedraw = true );

    void DisposeItemList( void );
    void SetViewDirty( void );
    void RecalculateItemExtents( void );

    long Draw( void );

#if defined(__WXMAC__)
    virtual void MacControlUserPaneActivateProc(
        bool                bActivating );
#endif


protected:
    // called by all ctors
    void Init( void );

    // event handlers
    void OnPaint( wxPaintEvent &event );
    void OnClick( wxMouseEvent &event );
    void OnDoubleClick( wxMouseEvent &event );
    void OnMouseMotion( wxMouseEvent &event );
    void OnMouseLeave ( wxMouseEvent &event );
    
    // event generator for "private" events
    void GenerateSelfEvent( wxEventType eventType );

protected:
    wxRect                  m_NativeBoundsR;
    wxSize                  m_defaultItemSize;
    wxColour                m_selectionColour;
    wxColumnHeaderItem      **m_itemList;
    long                    m_itemCount;
    long                    m_itemViewBaseIndex;
    long                    m_itemViewBaseOrigin;
    long                    m_itemSelected;
    bool                    m_bUseVerticalOrientation;  // false is horizontal (default)
    bool                    m_bUseGenericRenderer;      // Mac,MSW: either true or false; otherwise: always true
    bool                    m_bFixedHeight;         // Mac,MSW: opposite of m_bUseGenericRenderer; otherwise: always false
    bool                    m_bProportionalResizing;
    bool                    m_bVisibleSelection;
    int                     m_lastHot;

    // NB: same as wxGrid[Row,Col]LabelWindow
    DECLARE_DYNAMIC_CLASS(wxColumnHeader)
    DECLARE_EVENT_TABLE()
    DECLARE_NO_COPY_CLASS(wxColumnHeader)
};

class wxColumnHeaderItem
{
friend class wxColumnHeader;

public:
    wxColumnHeaderItem( const wxColumnHeaderItem *info );
    wxColumnHeaderItem();
    virtual ~wxColumnHeaderItem();

    long HitTest( const wxPoint &locationPt ) const;

    void GetItemData( wxColumnHeaderItem       *info ) const;
    void SetItemData( const wxColumnHeaderItem *info );

    wxBitmap GetLabelBitmap() const;
    void SetLabelBitmap( const wxBitmap  &targetBitmap );
    
    wxString GetLabelText() const;
    void SetLabelText( const wxString &textBuffer );
    wxAlignment GetLabelAlignment( void ) const;
    void SetLabelAlignment( wxAlignment align );

    wxPoint GetPosition( void ) const;
    void SetPosition( const wxPoint &targetPos );
    
    wxSize GetSize( void ) const;
    void SetSize( const wxSize &targetSize );

    bool GetAttribute( wxColumnHeaderItemAttribute flagEnum ) const;
    bool SetAttribute( wxColumnHeaderItemAttribute flagEnum, bool bFlagValue );

    long DrawItem( wxWindow        *parent,
                   wxDC&           dc,
                   const wxRect&   bounds,
                   const wxColour& selectionColour,
                   bool            visibleSelection,
                   bool useGeneric );
    
    void ResizeToWidth( long width );


protected:
    wxString    m_labelText;
    wxAlignment m_alignment;
    wxBitmap    m_bitmap;
    wxPoint     m_pos;
    wxSize      m_size;
    bool        m_visible;
    bool        m_enabled;
    bool        m_selected;
    bool        m_sortEnabled;
    bool        m_sortAscending;
    bool        m_fixedWidth;
    bool        m_isHot;
};


#endif // _WX_GENERIC_COLUMNHEADER_H
