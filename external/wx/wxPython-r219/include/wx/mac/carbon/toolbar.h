/////////////////////////////////////////////////////////////////////////////
// Name:        wx/mac/carbon/toolbar.h
// Purpose:     wxToolBar class
// Author:      Stefan Csomor
// Modified by:
// Created:     1998-01-01
// RCS-ID:      $Id: toolbar.h 44317 2007-01-25 23:35:07Z RD $
// Copyright:   (c) Stefan Csomor
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_TOOLBAR_H_
#define _WX_TOOLBAR_H_

#if wxUSE_TOOLBAR

#include "wx/tbarbase.h"
#include "wx/dynarray.h"

WXDLLEXPORT_DATA(extern const wxChar) wxToolBarNameStr[];

class WXDLLEXPORT wxToolBar: public wxToolBarBase
{
  DECLARE_DYNAMIC_CLASS(wxToolBar)
 public:
  /*
   * Public interface
   */

   wxToolBar() { Init(); }

  inline wxToolBar(wxWindow *parent, wxWindowID id,
                   const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
                   long style = wxNO_BORDER|wxTB_HORIZONTAL,
                   const wxString& name = wxToolBarNameStr)
  {
    Init();
    Create(parent, id, pos, size, style, name);
  }
  virtual ~wxToolBar();

  bool Create(wxWindow *parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,
            long style = wxNO_BORDER|wxTB_HORIZONTAL,
            const wxString& name = wxToolBarNameStr);

    virtual void SetWindowStyleFlag(long style);

    // override/implement base class virtuals
    virtual wxToolBarToolBase *FindToolForPosition(wxCoord x, wxCoord y) const;

    virtual bool Show(bool show = true);
    virtual bool IsShown() const;
    virtual bool Realize();

    virtual void SetToolBitmapSize(const wxSize& size);
    virtual wxSize GetToolSize() const;

    virtual void SetRows(int nRows);

#if wxABI_VERSION >= 20802
    // TODO: In 2.9 these should probably be virtual, and declared in the base class...
    void SetToolNormalBitmap(int id, const wxBitmap& bitmap);
    void SetToolDisabledBitmap(int id, const wxBitmap& bitmap);
#endif
#if wxABI_VERSION >= 20803
    void SetToolLabel(int id, const wxString& label);
#endif

    // Add all the buttons

    virtual wxString MacGetToolTipString( wxPoint &where ) ;
    void OnPaint(wxPaintEvent& event) ;
    void OnMouse(wxMouseEvent& event) ;
    virtual void MacSuperChangedPosition() ;

#if wxMAC_USE_NATIVE_TOOLBAR
    bool MacInstallNativeToolbar(bool usesNative);
    bool MacWantsNativeToolbar();
    bool MacTopLevelHasNativeToolbar(bool *ownToolbarInstalled) const;
#endif
protected:
    // common part of all ctors
    void Init();

    virtual void DoGetSize(int *width, int *height) const;
    virtual wxSize DoGetBestSize() const;
    virtual bool DoInsertTool(size_t pos, wxToolBarToolBase *tool);
    virtual bool DoDeleteTool(size_t pos, wxToolBarToolBase *tool);

    virtual void DoEnableTool(wxToolBarToolBase *tool, bool enable);
    virtual void DoToggleTool(wxToolBarToolBase *tool, bool toggle);
    virtual void DoSetToggle(wxToolBarToolBase *tool, bool toggle);

    virtual wxToolBarToolBase *CreateTool(int id,
                                          const wxString& label,
                                          const wxBitmap& bmpNormal,
                                          const wxBitmap& bmpDisabled,
                                          wxItemKind kind,
                                          wxObject *clientData,
                                          const wxString& shortHelp,
                                          const wxString& longHelp);
    virtual wxToolBarToolBase *CreateTool(wxControl *control);

    DECLARE_EVENT_TABLE()
#if wxMAC_USE_NATIVE_TOOLBAR
    bool m_macUsesNativeToolbar ;
    void* m_macHIToolbarRef ;
#endif
};

class WXDLLEXPORT wxToolBarTool : public wxToolBarToolBase
{
public:
    wxToolBarTool(
        wxToolBar *tbar = (wxToolBar *)NULL,
        int id = wxID_SEPARATOR,
        const wxString& label = wxEmptyString,
        const wxBitmap& bmpNormal = wxNullBitmap,
        const wxBitmap& bmpDisabled = wxNullBitmap,
        wxItemKind kind = wxITEM_NORMAL,
        wxObject *clientData = (wxObject *) NULL,
        const wxString& shortHelp = wxEmptyString,
        const wxString& longHelp = wxEmptyString );

    wxToolBarTool(wxToolBar *tbar, wxControl *control);

    virtual ~wxToolBarTool()
    {
        ClearControl();
    }

    WXWidget GetControlHandle()
    {
        return m_controlHandle;
    }

    void SetControlHandle( WXWidget handle )
    {
        m_controlHandle = handle;
    }

    void SetPosition( const wxPoint& position );

    void ClearControl();

    wxSize GetSize() const;

    wxPoint GetPosition() const
    {
        return wxPoint( m_x, m_y );
    }

    bool DoEnable( bool enable );

    void UpdateToggleImage( bool toggle );

#if wxMAC_USE_NATIVE_TOOLBAR
    void SetToolbarItemRef( WXWidget ref );

    WXWidget GetToolbarItemRef() const
    {
        return m_toolbarItemRef;
    }

    void SetIndex( int index )
    {
        m_index = index;
    }

    int GetIndex() const
    {
        return m_index;
    }
#endif

private:
    void Init();

    WXWidget m_controlHandle;
    wxCoord     m_x;
    wxCoord     m_y;

#if wxMAC_USE_NATIVE_TOOLBAR
    WXWidget m_toolbarItemRef;
    // position in its toolbar, -1 means not inserted
    int m_index;
#endif
  DECLARE_DYNAMIC_CLASS_NO_COPY(wxToolBarTool)
};


#endif // wxUSE_TOOLBAR

#endif
    // _WX_TOOLBAR_H_
