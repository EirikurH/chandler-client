///////////////////////////////////////////////////////////////////////////////
// Name:        src/generic/colheader.cpp
// Purpose:     2-platform (Mac,MSW) + generic implementation of a native-appearance column header
// Author:      David Surovell
// Modified by:
// Created:     01.01.2005
// RCS-ID:
// Copyright:
// License:
///////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#if defined(__BORLANDC__)
    #pragma hdrstop
#endif

#if !defined(WX_PRECOMP)
    #include "wx/settings.h"
    #include "wx/listbox.h"
    #include "wx/dcclient.h"
    #include "wx/dcmemory.h"
    #include "wx/bitmap.h"
    #include "wx/gdicmn.h"
    #include "wx/colour.h"
    #include "wx/app.h"      // for wxTheApp
#endif

#if defined(__WXMAC__)
    #include "wx/mac/uma.h"
#endif

#include "wx/renderer.h"
#include "wx/colheader.h"


// ----------------------------------------------------------------------------
// wx binding macros
// ----------------------------------------------------------------------------

#if wxUSE_EXTENDED_RTTI
WX_DEFINE_FLAGS( wxColumnHeaderStyle )

wxBEGIN_FLAGS( wxColumnHeaderStyle )
    // new style border flags:
    // put them first to use them for streaming out
    wxFLAGS_MEMBER(wxBORDER_SIMPLE)
    wxFLAGS_MEMBER(wxBORDER_SUNKEN)
    wxFLAGS_MEMBER(wxBORDER_DOUBLE)
    wxFLAGS_MEMBER(wxBORDER_RAISED)
    wxFLAGS_MEMBER(wxBORDER_STATIC)
    wxFLAGS_MEMBER(wxBORDER_NONE)

    // old style border flags
    wxFLAGS_MEMBER(wxSIMPLE_BORDER)
    wxFLAGS_MEMBER(wxSUNKEN_BORDER)
    wxFLAGS_MEMBER(wxDOUBLE_BORDER)
    wxFLAGS_MEMBER(wxRAISED_BORDER)
    wxFLAGS_MEMBER(wxSTATIC_BORDER)
    wxFLAGS_MEMBER(wxBORDER)

    // standard window styles
    wxFLAGS_MEMBER(wxTAB_TRAVERSAL)
    wxFLAGS_MEMBER(wxCLIP_CHILDREN)
    wxFLAGS_MEMBER(wxTRANSPARENT_WINDOW)
    wxFLAGS_MEMBER(wxWANTS_CHARS)
    wxFLAGS_MEMBER(wxFULL_REPAINT_ON_RESIZE)
    wxFLAGS_MEMBER(wxALWAYS_SHOW_SB)
wxEND_FLAGS( wxColumnHeaderStyle )

IMPLEMENT_DYNAMIC_CLASS_XTI(wxColumnHeader, wxControl, "wx/colheader.h")

wxBEGIN_PROPERTIES_TABLE(wxColumnHeader)
    wxEVENT_RANGE_PROPERTY( Updated, wxEVT_COLUMNHEADER_SELCHANGED, wxEVT_COLUMNHEADER_DOUBLECLICKED, wxColumnHeaderEvent )
    wxHIDE_PROPERTY( Children )
    wxPROPERTY( 0 /*flags*/ , wxT("Helpstring"), wxT("group"))
    wxPROPERTY_FLAGS( WindowStyle, wxColumnHeaderStyle, long, SetWindowStyleFlag, GetWindowStyleFlag, , 0 /*flags*/, wxT("Helpstring"), wxT("group") ) // style
wxEND_PROPERTIES_TABLE()

wxBEGIN_HANDLERS_TABLE(wxColumnHeader)
wxEND_HANDLERS_TABLE()

wxCONSTRUCTOR_5( wxColumnHeader, wxWindow*, Parent, wxWindowID, Id, wxPoint, Position, wxSize, Size, long, WindowStyle )

#else
IMPLEMENT_DYNAMIC_CLASS(wxColumnHeader, wxControl)
#endif

IMPLEMENT_DYNAMIC_CLASS(wxColumnHeaderEvent, wxCommandEvent)


// ----------------------------------------------------------------------------
// events
// ----------------------------------------------------------------------------

DEFINE_EVENT_TYPE(wxEVT_COLUMNHEADER_SELCHANGED)
DEFINE_EVENT_TYPE(wxEVT_COLUMNHEADER_DOUBLECLICKED)

BEGIN_EVENT_TABLE( wxColumnHeader, wxControl )
    EVT_PAINT(        wxColumnHeader::OnPaint )
    EVT_LEFT_DOWN(    wxColumnHeader::OnClick )
    EVT_LEFT_DCLICK(  wxColumnHeader::OnDoubleClick )
    EVT_MOTION(       wxColumnHeader::OnMouseMotion )
    EVT_LEAVE_WINDOW( wxColumnHeader::OnMouseLeave )
END_EVENT_TABLE()


// ============================================================================
// implementation
// ============================================================================

const int wxCH_minimumTotalX          = 16;
const int wxCH_minimumTotalY          = 17;

// ----------------------------------------------------------------------------
// wxColumnHeader
// ----------------------------------------------------------------------------

wxColumnHeader::wxColumnHeader()
{
    Init();
}


wxColumnHeader::wxColumnHeader(wxWindow            *parent,
                               wxWindowID          id,
                               const wxPoint       &pos,
                               const wxSize        &size,
                               long                style,
                               const wxString      &name )
{
    Init();
    (void)Create( parent, id, pos, size, style, name );
}


wxColumnHeader::~wxColumnHeader()
{
    DisposeItemList();
}


void wxColumnHeader::Init( void )
{
    m_NativeBoundsR.x =
    m_NativeBoundsR.y =
    m_NativeBoundsR.width =
    m_NativeBoundsR.height = 0;

    m_defaultItemSize.x =
    m_defaultItemSize.y = 0;

    m_itemList = NULL;
    m_itemCount = 0;
    m_itemViewBaseIndex = 0;
    m_itemViewBaseOrigin = 0;
    m_itemSelected = CH_HITTEST_NoPart;

    m_selectionColour.Set( 0x66, 0x66, 0x66 );

    m_bUseVerticalOrientation = false;

    m_bFixedHeight = true;
    m_bUseGenericRenderer = false;

    m_bProportionalResizing = true;
    m_bVisibleSelection = true;

    m_lastHot = -1;
}


bool wxColumnHeader::Create( wxWindow            *parent,
                             wxWindowID          id,
                             const wxPoint       &pos,
                             const wxSize        &size,
                             long                style,
                             const wxString      &name )
{
    wxSize      actualSize;
    bool        bResultV;

    style |= wxBORDER_NONE;
    bResultV =  wxControl::Create( parent, id, pos, size,
                                   style, wxDefaultValidator, name );

    m_defaultItemSize = CalculateDefaultItemSize( size );
    
    actualSize = size;
    
#if 1
    if (m_bFixedHeight)
    {
        actualSize = CalculateDefaultSize();
        if (size.x > 0)
            actualSize.x = size.x;
    }
#else
    if ((actualSize.x <= 0) || (m_bFixedHeight && (actualSize.x > 0)))
        actualSize.x = m_defaultItemSize.x;
    if (actualSize.y <= 0)
        actualSize.y = m_defaultItemSize.y;
#endif

    
    wxFont font = wxSystemSettings::GetFont( wxSYS_DEFAULT_GUI_FONT );
#if defined(__WXMAC__)
    // NB: or kThemeSystemFontTag, kThemeViewsFontTag
    font.MacCreateThemeFont( kThemeSmallSystemFont );
#endif
    SetFont(font);
    
    // NB: we're stealing a bit in the style argument from Win32 and wx to support ListHeader attributes
    // assumes CH_STYLE_HeaderIsVertical is integral power of two value
    if (style & CH_STYLE_HeaderIsVertical)
    {
        m_bUseVerticalOrientation = true;
        style &= ~CH_STYLE_HeaderIsVertical;
    }


    SetInitialSize( actualSize );

    // NB: is this advisable?
    wxControl::DoGetPosition( &(m_NativeBoundsR.x), &(m_NativeBoundsR.y) );
    wxControl::DoGetSize( &(m_NativeBoundsR.width), &(m_NativeBoundsR.height) );

    return bResultV;
}


// virtual
bool wxColumnHeader::Enable(bool bEnable )
{
    long        i;
    bool        bResultV;

    if (bEnable == IsEnabled())
        return false;

    bResultV = wxControl::Enable( bEnable );

    if (bResultV )
        for (i=0; i<m_itemCount; i++)
        {
            if ((m_itemList != NULL) && (m_itemList[i] != NULL))
                m_itemList[i]->SetAttribute( CH_ITEM_ATTR_Enabled, bEnable );
            RefreshItem( i, false );
        }

    // force a redraw
    SetViewDirty();             // TODO: is this needed if all items are refreshed?

    return bResultV;
}

// ----------------------------------------------------------------------------
// size management
// ----------------------------------------------------------------------------

// virtual
void wxColumnHeader::DoMoveWindow(int x, int y, int width, int height )
{
    // TODO, is this function really needed?
    
    int     yDiff;

    yDiff = 0;

    wxControl::DoMoveWindow( x, y + yDiff, width, height - yDiff );

    // NB: is this advisable?
    wxControl::DoGetPosition( &(m_NativeBoundsR.x), &(m_NativeBoundsR.y) );
}


// virtual
wxSize wxColumnHeader::DoGetBestSize( void ) const
{
    wxSize  targetSize;

    targetSize = CalculateDefaultSize();
    CacheBestSize( targetSize );

    return targetSize;
}


// virtual
wxSize wxColumnHeader::DoGetMinSize( void ) const
{
    wxSize  targetSize;

    targetSize = CalculateDefaultSize();
    targetSize.x = 0;

    return targetSize;
}



// TODO: would this be better handled by an EVT_SIZE handler?

// virtual
void wxColumnHeader::DoSetSize( int     x,           
                                int     y,
                                int     width,
                                int     height,
                                int     sizeFlags )
{
    // FIXME: should be - invalidate( origBoundsR )

    // NB: correct height for native platform limitations as needed
    if (!m_bUseVerticalOrientation && m_bFixedHeight)
    {
        wxSize      actualSize;

        actualSize = CalculateDefaultSize();
        height = actualSize.y;
    }

    wxControl::DoSetSize( x, y, width, height, sizeFlags );

    if (m_bProportionalResizing)
        RescaleToFit( width );

    // NB: is this advisable?
    wxControl::DoGetPosition( &(m_NativeBoundsR.x), &(m_NativeBoundsR.y) );
    wxControl::DoGetSize( &(m_NativeBoundsR.width), &(m_NativeBoundsR.height) );

    // RecalculateItemExtents();
    SetViewDirty();
}


wxSize wxColumnHeader::CalculateDefaultSize( void ) const
{
    wxWindow    *parentW;
    wxSize      targetSize, itemSize, minSize, parentSize;
    bool        bIsVertical;

    targetSize.x =
    targetSize.y = 0;

    minSize.x = wxCH_minimumTotalX;
    minSize.y = wxCH_minimumTotalY;

    parentSize.x =
    parentSize.y = 0;

    // "best" width is parent's width;
    // height is (relatively) invariant,
    // as determined by native (HI/CommonControls) drawing routines
    parentW = GetParent();
    if (parentW != NULL)
        parentW->GetClientSize( &(parentSize.x), &(parentSize.y) );

    itemSize = GetDefaultItemSize();

    bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );
    if (bIsVertical)
    {
        targetSize.x = parentSize.x;  // TODO: should this be itemSize.x?
        targetSize.y = parentSize.y;
    }
    else
    {
        targetSize.x = parentSize.x;
        targetSize.y = itemSize.y;
    }

    targetSize.x = ((targetSize.x > minSize.x) ? targetSize.x : minSize.x);
    targetSize.y = ((targetSize.y > minSize.y) ? targetSize.y : minSize.y);

#if 0
    if (! HasFlag( wxBORDER_NONE ))
    {
        // the border would clip the last line otherwise
        targetSize.x += 4;
        targetSize.y += 6;
    }
#endif

    return targetSize;
}


// static
long wxColumnHeader::GetFixedHeight( void )
{
    // TODO: Is there a better window to use for this?  Or can we allow
    // GetHeaderButtonHeight to not need a window?  Or can we just get rid of
    // this GetFixedHeight static function?
    return wxRendererNative::Get().GetHeaderButtonHeight(wxTheApp->GetTopWindow());
}


wxSize wxColumnHeader::CalculateDefaultItemSize( const wxSize  &maxSize )
{
    wxSize      targetSize;

    // "best" width is parent's width;
    // height is (relatively) invariant,
    // as determined by native (HI/CommonControls) drawing routines
    targetSize.x = (maxSize.x > 0) ? maxSize.x : 0;
    targetSize.y = 0;

    targetSize.y = wxRendererNative::Get().GetHeaderButtonHeight(this);    
    return targetSize;
}


wxSize wxColumnHeader::GetDefaultItemSize( void ) const
{
    return m_defaultItemSize;
}


void wxColumnHeader::SetDefaultItemSize( const wxSize& targetSize )
{
    if (targetSize.x > 0)
        m_defaultItemSize.x = targetSize.x;
    if (targetSize.y > 0)
        m_defaultItemSize.y = targetSize.y;
}


// static
void wxColumnHeader::GetDefaultLabelValue( bool            isVertical,
                                           long            itemIndex,
                                           wxString        &value )
{
    if (isVertical)
    {
        // starting the rows at zero confuses users,
        // no matter how much it makes sense to geeks.
        value.Format( wxT("%ld"), itemIndex + 1 );
    }
    else
    {
        // default column labels are:
        // columns 0 to 25: A-Z
        // columns 26 to 675: AA-ZZ
        // and so on
        wxString    s;
        long        i, n, reverseIndex;

        for (n = 1; itemIndex >= 0; n++)
        {
            s += (wxChar)(wxT('A') + (wxChar)(itemIndex % 26));
            itemIndex /= 26;
            itemIndex--;
        }

        // reverse the string
        value = wxEmptyString;
        for (i = 0; i < n; i++)
        {
            reverseIndex = (n - i) - 1L;
            value += s[(int)reverseIndex];
        }
    }
}


// static
wxVisualAttributes wxColumnHeader::GetClassDefaultAttributes( wxWindowVariant variant )
{
    // FIXME: is this dependency necessary?
    // use the same color scheme as wxListBox
    return wxListBox::GetClassDefaultAttributes( variant );
}


// ----------------------------------------------------------------------------
// event handlers
// ----------------------------------------------------------------------------

// virtual
void wxColumnHeader::OnPaint( wxPaintEvent& WXUNUSED(event) )
{
    Draw();
}


void wxColumnHeader::OnDoubleClick( wxMouseEvent& event )
{
    long        itemIndex;

    itemIndex = HitTest( event.GetPosition() );
    if (itemIndex >= CH_HITTEST_ItemZero)
    {
        // NB: just call the single click handler for the present
        OnClick( event );

        // NB: unused for the present
        //GenerateSelfEvent( wxEVT_COLUMNHEADER_DOUBLECLICKED );
    }
    else
    {
        event.Skip();
    }
}

void wxColumnHeader::OnClick( wxMouseEvent& event )
{
    long        itemIndex;

    itemIndex = HitTest( event.GetPosition() );
    switch (itemIndex)
    {
        default:
            if (itemIndex >= CH_HITTEST_ItemZero)
            {
                if (IsEnabled())
                {
                    OnClick_SelectOrToggleSort( itemIndex, true );
                    GenerateSelfEvent( wxEVT_COLUMNHEADER_SELCHANGED );
                }
                break;
            }
            else
            {
            // unknown message - unhandled - fall through
            //wxLogDebug( wxT("wxColumnHeader::OnClick - unknown hittest code") );
            }

        case CH_HITTEST_NoPart:
            event.Skip();
            break;
    }
}


void wxColumnHeader::OnClick_SelectOrToggleSort( long  itemIndex,
                                                 bool  bToggleSortDirection )
{
    long curSelectionIndex;

    curSelectionIndex = GetSelectedItem();
    if (itemIndex != m_itemSelected)
    {
        SetSelectedItem( itemIndex );
    }
    else if (bToggleSortDirection)
    {
        wxColumnHeaderItem  *item;
        bool                bSortFlag;

        item = ((m_itemList != NULL) ? m_itemList[itemIndex] : NULL);
        if (item != NULL)
            if (item->GetAttribute( CH_ITEM_ATTR_SortEnabled ))
            {
                bSortFlag = item->GetAttribute( CH_ITEM_ATTR_SortDirection );
                item->SetAttribute( CH_ITEM_ATTR_SortDirection, ! bSortFlag );

                RefreshItem( itemIndex, true );
            }
    }
}


void wxColumnHeader::OnMouseMotion( wxMouseEvent& event )
{
    wxRect bounds;
    wxPoint pos = event.GetPosition();
    int item = HitTest(pos);
    
    if ( item >= 0 && m_lastHot != item )
    {
        // draw this item with the hotTracking enabled
        m_itemList[item]->m_isHot = true;
        GetItemBounds(item, &bounds);
        RefreshRect(bounds, false);

        // and redraw the last hot item normally
        if ( m_lastHot >= 0)
        {
            m_itemList[m_lastHot]->m_isHot = false;
            GetItemBounds(m_lastHot, &bounds);
            RefreshRect(bounds, false);
        }

        Update();
        m_lastHot = item;
    }
}


void wxColumnHeader::OnMouseLeave( wxMouseEvent& WXUNUSED(event) )
{   
    // The mouse has left the control so reset the last hot item 
    if ( m_lastHot >= 0)
    {
        wxRect bounds;
        
        m_itemList[m_lastHot]->m_isHot = false;
        GetItemBounds(m_lastHot, &bounds);
        RefreshRect(bounds, false);
        Update();
        m_lastHot = -1;
    }
}


wxColour wxColumnHeader::GetSelectionColour() const
{
    return m_selectionColour;
}


void wxColumnHeader::SetSelectionColour( const wxColour& targetColour )
{
    m_selectionColour = targetColour;
}



bool wxColumnHeader::GetAttribute( wxColumnHeaderAttribute flagEnum ) const
{
    bool bResult = false;

    switch (flagEnum)
    {
        case CH_ATTR_VerticalOrientation:
            bResult = m_bUseVerticalOrientation;
            break;

        case CH_ATTR_GenericRenderer:
            bResult = m_bUseGenericRenderer;
            break;

        case CH_ATTR_FixedHeight:
            bResult = m_bFixedHeight;
            break;
    
        case CH_ATTR_ProportionalResizing:
            bResult = m_bProportionalResizing;
            break;
    
        case CH_ATTR_VisibleSelection:
            bResult = m_bVisibleSelection;
            break;

        default:
            break;
    }

    return bResult;
}


bool wxColumnHeader::SetAttribute( wxColumnHeaderAttribute flagEnum,
                                   bool                    bFlagValue )
{
    bool bResult = true;

    switch (flagEnum)
    {
        case CH_ATTR_VerticalOrientation:
            // NB: runtime assignment not (currently) supported
            // m_bUseVerticalOrientation = bFlagValue;
            break;
    
        case CH_ATTR_GenericRenderer:
            if (m_bUseGenericRenderer != bFlagValue)
            {
                m_bUseGenericRenderer = bFlagValue;
                SetViewDirty();
            }
            break;

        case CH_ATTR_FixedHeight:
            // NB: runtime assignment not (currently) supported
            // m_bFixedHeight = bFlagValue;
            break;
    
        case CH_ATTR_ProportionalResizing:
            if (m_bProportionalResizing != bFlagValue)
                m_bProportionalResizing = bFlagValue;
            break;
    
        case CH_ATTR_VisibleSelection:
            if (m_bVisibleSelection != bFlagValue)
            {
                m_bVisibleSelection = bFlagValue;

                if (m_itemSelected >= 0)
                    RefreshItem( m_itemSelected, true );
            }
            break;

        default:
            bResult = false;
            break;
    }

    return bResult;
}



// ----------------------------------------------------------------------------
// utility
// ----------------------------------------------------------------------------

wxSize wxColumnHeader::GetTotalUIExtent( long itemCount,
                                         bool bStartAtBase ) const
{
    wxSize  extentDim;
    long    i, startItem;

    if ((itemCount < 0) || (itemCount > m_itemCount))
        itemCount = m_itemCount;

    extentDim.x =
    extentDim.y = 0;

    startItem = 0;
    if (bStartAtBase)
        startItem = 0;

    if (m_itemList != NULL)
        for (i=0; i<itemCount; i++)
        {
            if (m_itemList[i] != NULL)
                if (m_itemList[i]->m_visible)
                    extentDim += m_itemList[i]->m_size;
        }

    return extentDim;
}

// NB: ignores current view range
//
bool wxColumnHeader::RescaleToFit( long newWidth )
{
    long scaleItemCount, scaleItemAmount, i;
    long deltaX, summerX, resultX, originX, incX;
    bool bIsVertical;

    if ((newWidth <= 0) || (m_itemList == NULL))
        return false;

    // FIXME: needs work for vertical row headers
    bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );
    if (bIsVertical)
        return false;

    // count visible, non-fixed-width items and tabulate size
    scaleItemCount = 0;
    scaleItemAmount = 0;
    for (i=0; i<m_itemCount; i++)
    {
        if ((m_itemList[i] == NULL) || !m_itemList[i]->m_visible || m_itemList[i]->m_fixedWidth)
            continue;

        scaleItemCount++;
        scaleItemAmount += m_itemList[i]->m_size.x;
    }

    // determine width delta
    deltaX = newWidth - m_NativeBoundsR.width;
    if ((deltaX == 0) || (scaleItemAmount <= 0))
        return true;

    summerX = deltaX;
    originX = 0;

    // move and resize items as appropriate
    for (i=0; i<m_itemCount; i++)
    {
        if (m_itemList[i] == NULL)
            continue;

        // move to new origin
        m_itemList[i]->m_pos.x = originX;
//      m_itemList[i]->m_pos = origin;

        // resize item, if non-fixed
        if (m_itemList[i]->m_visible && !m_itemList[i]->m_fixedWidth)
        {
            scaleItemCount--;

            if (scaleItemCount > 0)
                incX = (deltaX * m_itemList[i]->m_size.x) / scaleItemAmount;
            else
                incX = summerX;

            if (incX != 0)
            {
                resultX = m_itemList[i]->m_size.x + incX;
                m_itemList[i]->ResizeToWidth( resultX );

                summerX -= incX;
            }
        }

        // advance current origin
        originX += m_itemList[i]->m_size.x;
    }

    for (i=0; i<m_itemCount; i++)
        RefreshItem( i, false );
    SetViewDirty();

    return true;
}


bool wxColumnHeader::ResizeToFit( long itemCount )
{
    wxSize  extentV;
    bool    bIsVertical, bScaling;

    if ((itemCount < 0) || (itemCount > m_itemCount))
        itemCount = m_itemCount;

    bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );
    if (bIsVertical)
    {
        extentV.y = itemCount * m_defaultItemSize.y;
        DoSetSize( m_NativeBoundsR.x, m_NativeBoundsR.y, m_defaultItemSize.x, extentV.y, 0 );
    }
    else
    {
        // temporarily turn off proportional resizing
        bScaling = m_bProportionalResizing;
        m_bProportionalResizing = false;

        extentV = GetTotalUIExtent( itemCount );
        DoSetSize( m_NativeBoundsR.x, m_NativeBoundsR.y, extentV.x, m_NativeBoundsR.height, 0 );

        if (bScaling)
            m_bProportionalResizing = true;
    }

    return true;
}


bool wxColumnHeader::ResizeDivision( long itemIndex,
                                     long originX )
{
    wxColumnHeaderItem *itemRef1, *itemRef2;
    long               deltaV, newExtent1, newExtent2;
    bool               bIsVertical;

    if ((itemIndex <= 0) || (itemIndex >= m_itemCount))
        return false;

    // FIXME: needs work for vertical row headers;
    // may not be meaningful because Y dimension is assumed to be invariant
    bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );
    if (bIsVertical)
        return false;

    itemRef1 = GetItemRef( itemIndex - 1 );
    itemRef2 = GetItemRef( itemIndex );
    if ((itemRef1 == NULL) || (itemRef2 == NULL))
        return false;

//  if (bIsVertical)
//  {
//      if ((originY <= itemRef1->m_OriginY) || (originY >= itemRef2->m_OriginY + itemRef2->m_sizeY))
//          return false;
//  }
//  else
    {
        if ((originX <= itemRef1->m_pos.x) || (originX >= itemRef2->m_pos.x + itemRef2->m_size.x))
            return false;
    }

    deltaV = itemRef2->m_pos.x - originX;
    newExtent1 = itemRef1->m_size.x - deltaV;
    newExtent2 = itemRef2->m_size.x + deltaV;

    itemRef2->m_pos.x = itemRef1->m_pos.x + newExtent1;
    itemRef1->ResizeToWidth( newExtent1 );
    itemRef2->ResizeToWidth( newExtent2 );

    RefreshItem( itemIndex - 1, true );
    RefreshItem( itemIndex, true );

    return true;
}


long wxColumnHeader::GetSelectedItem( void ) const
{
    return m_itemSelected;
}


void wxColumnHeader::SetSelectedItem( long itemIndex )
{
    long  i;
    bool  bSelected;

    if (m_itemSelected != itemIndex)
    {
        for (i=0; i<m_itemCount; i++)
        {
            bSelected = (i == itemIndex);
            if ((m_itemList != NULL) && (m_itemList[i] != NULL))
                m_itemList[i]->SetAttribute( CH_ITEM_ATTR_Selected, bSelected );

            RefreshItem( i, false );
        }

        m_itemSelected = itemIndex;

        SetViewDirty();
    }
}


bool wxColumnHeader::GetItemVisibility( long itemIndex ) const
{
    wxColumnHeaderItem      *itemRef;
    bool                    bResultV;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);
    if (bResultV)
        bResultV = itemRef->m_visible;

    return bResultV;
}


void wxColumnHeader::SetItemVisibility( long itemIndex,
                                        bool bVisible )
{
    wxColumnHeaderItem      *itemRef;

    if ((itemIndex < 0) || (itemIndex >= m_itemCount))
        return;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
        if (itemRef->m_visible != bVisible)
        {
            itemRef->m_visible = bVisible;
            if (ItemInView( itemIndex ))
                SetViewDirty();
        }
}


long wxColumnHeader::GetBaseViewItem( void ) const
{
    return m_itemViewBaseIndex;
}


void wxColumnHeader::SetBaseViewItem( long itemIndex )
{
    if ((itemIndex < 0) || (itemIndex >= m_itemCount))
        return;

    if (m_itemViewBaseIndex != itemIndex)
    {
        m_itemViewBaseIndex = itemIndex;

        m_itemViewBaseOrigin = 0;
        if (itemIndex > 0)
        {
        wxColumnHeaderItem      *itemRef;

            // cache the value of the starting left/top edge
            // in order to simplify item bounds calculations
            itemRef = GetItemRef( itemIndex );
            if (itemRef != NULL)
                m_itemViewBaseOrigin = itemRef->m_pos.x;
        }

        RecalculateItemExtents();
        SetViewDirty();
    }
}


long wxColumnHeader::GetItemCount( void ) const
{
    return (long)m_itemCount;
}

void wxColumnHeader::SetItemCount( long itemCount )
{
    if (itemCount < 0)
        itemCount = 0;

    if (itemCount > m_itemCount)
        AddEmptyItems( -1, itemCount - m_itemCount );
    else if (itemCount < m_itemCount)
        DeleteItems( itemCount, m_itemCount - itemCount );
}


void wxColumnHeader::DeleteItems( long itemIndex,
                                  long itemCount )
{
    long        i;

    for (i=0; i<itemCount; i++)
        DeleteItem( itemIndex );
}


void wxColumnHeader::DeleteItem( long itemIndex )
{
    long        i;

    if ((itemIndex >= 0) && (itemIndex < m_itemCount))
    {
        if (m_itemList != NULL)
        {
            if (m_itemCount > 1)
            {
                // delete the target item
                delete m_itemList[itemIndex];

                // close the list hole
                for (i=itemIndex; i<m_itemCount-1; i++)
                    m_itemList[i] = m_itemList[i + 1];

                // leave a NULL spot at the end
                m_itemList[m_itemCount - 1] = NULL;
                m_itemCount--;

                // recalculate item origins
                RecalculateItemExtents();
            }
            else
                DisposeItemList();

            // if this item was selected, then there is a selection no longer
            if (m_itemSelected == itemIndex)
                m_itemSelected = CH_HITTEST_NoPart;

            SetViewDirty();
        }
    }
}


void wxColumnHeader::AppendItem( const wxString &textBuffer,
                                 wxAlignment    align,
                                 long           extentX,
                                 bool           bSelected,
                                 bool           bSortEnabled,
                                 bool           bSortAscending )
{
    AddItem( -1, textBuffer, align, extentX, bSelected, bSortEnabled, bSortAscending );
}


void wxColumnHeader::AddEmptyItems( long beforeIndex,
                                    long itemCount )
{
    wxAlignment targetAlign = wxALIGN_LEFT;
    long        i;

    for (i=0; i<itemCount; i++)
        AddItem( beforeIndex, wxEmptyString, targetAlign, -1, false, true, true );
}

void wxColumnHeader::AddItem( long           beforeIndex,
                              const wxString &textBuffer,
                              wxAlignment    align,
                              long           extentX,
                              bool           bSelected,
                              bool           bSortEnabled,
                              bool           bSortAscending )
{
    wxColumnHeaderItem itemInfo;
    wxString           labelValue;
    wxPoint            targetPos;
    wxSize             targetExtent;
    long               curIndex, originX;
    bool               bIsVertical;

    // set (initially) invariant values
    itemInfo.m_visible = true;
    itemInfo.m_enabled = true;
    itemInfo.m_pos.y = 0;
    itemInfo.m_size.y = 0;

    // set default-specified values
    bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );
    if (extentX < 0)
    {
        if (bIsVertical)
            extentX = m_defaultItemSize.x;
        else
            extentX = m_defaultItemSize.y;
    }

    // if needed, determine label value
    if (textBuffer.IsEmpty())
    {
        curIndex = ((beforeIndex >= 0) && (beforeIndex < m_itemCount)) ? beforeIndex : m_itemCount;
        GetDefaultLabelValue( m_bUseVerticalOrientation, beforeIndex, labelValue );
    }
    else
        labelValue = textBuffer;

    // set specified values
    itemInfo.m_labelText = textBuffer;
    itemInfo.m_alignment = align;
    itemInfo.m_size.x = extentX;
    itemInfo.m_selected = ((m_itemSelected < 0) ? bSelected : false);
    itemInfo.m_sortEnabled = bSortEnabled && !bIsVertical;
    itemInfo.m_sortAscending = bSortAscending;

    // determine new item origin
    itemInfo.m_pos.x = 0;
    if ((beforeIndex < 0) || (beforeIndex > m_itemCount))
        beforeIndex = m_itemCount;
    if (beforeIndex > 0)
    {
        if (!bIsVertical)
        {
            targetPos = GetItemPosition( beforeIndex - 1 );
            targetExtent = GetItemSize( beforeIndex - 1 );
            originX = ((targetExtent.x > 0) ? targetExtent.x : 0);
            itemInfo.m_pos.x = originX + targetPos.x;
        }
    }

    AddItemList( &itemInfo, 1, beforeIndex );
}


void wxColumnHeader::AddItemList( const wxColumnHeaderItem *itemList,
                                  long                     itemCount,
                                  long                     beforeIndex )
{
    wxColumnHeaderItem  **newItemList;
    long                targetIndex, i;
    bool                bIsSelected;

    if ((itemList == NULL) || (itemCount <= 0))
        return;

    if ((beforeIndex < 0) || (beforeIndex > m_itemCount))
        beforeIndex = m_itemCount;

    // allocate new item list and copy the original list items into it
    newItemList = (wxColumnHeaderItem**)calloc( m_itemCount + itemCount, sizeof(wxColumnHeaderItem*) );
    if (m_itemList != NULL)
    {
        for (i=0; i<m_itemCount; i++)
        {
            targetIndex = ((i < beforeIndex) ? i : itemCount + i);
            newItemList[targetIndex] = m_itemList[i];
        }

        free( m_itemList );
    }
    m_itemList = newItemList;

    // append the new items
    for (i=0; i<itemCount; i++)
    {
        targetIndex = beforeIndex + i;
        m_itemList[targetIndex] = new wxColumnHeaderItem( &itemList[i] );

        bIsSelected = (m_itemList[targetIndex]->m_selected && m_itemList[targetIndex]->m_enabled);

        if (bIsSelected && (m_itemSelected < 0))
            m_itemSelected = targetIndex;
    }

    // update the item count
    m_itemCount += itemCount;

    // if this was an insertion, refresh the end items
    if (beforeIndex < m_itemCount - itemCount)
    {
        RecalculateItemExtents();
        for (i=0; i<itemCount; i++)
            RefreshItem( i + (m_itemCount - itemCount), false );
    }

    // if this moves the selection, reset it
    if (m_itemSelected >= beforeIndex)
    {
        long        savedIndex;

        savedIndex = m_itemSelected;
        m_itemSelected = CH_HITTEST_NoPart;
        SetSelectedItem( savedIndex );
    }

    SetViewDirty();
}


void wxColumnHeader::DisposeItemList( void )
{
    long        i;

    if (m_itemList != NULL)
    {
        for (i=0; i<m_itemCount; i++)
            delete m_itemList[i];

        free( m_itemList );
        m_itemList = NULL;
    }

    m_itemCount = 0;
    m_itemSelected = CH_HITTEST_NoPart;
}



bool wxColumnHeader::GetItemData( long               itemIndex,
                                  wxColumnHeaderItem *info ) const
{
    wxColumnHeaderItem      *itemRef;
    bool                    bResultV;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);
    if (bResultV)
        itemRef->GetItemData( info );

    return bResultV;
}


bool wxColumnHeader::SetItemData( long                     itemIndex,
                                  const wxColumnHeaderItem *info )
{
    wxColumnHeaderItem      *itemRef;
    bool                    bResultV;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);
    if (bResultV)
        itemRef->SetItemData( info );

    return bResultV;
}


bool wxColumnHeader::ItemInView( long itemIndex ) const
{
    wxColumnHeaderItem      *itemRef;
    bool                    bResultV;

    if (itemIndex < m_itemViewBaseIndex)
        return false;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);

    return bResultV;
}


bool wxColumnHeader::GetItemBounds( long   itemIndex,
                                    wxRect *boundsR ) const
{
    wxColumnHeaderItem      *itemRef;
    bool                    bResultV, bIsVertical;

    if (boundsR == NULL)
        return false;

    itemRef = NULL;
    bResultV = ItemInView( itemIndex );
    if (bResultV)
    {
        itemRef = GetItemRef( itemIndex );
        bResultV = (itemRef != NULL);
    }

    if (bResultV)
    {
        bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );
        if (bIsVertical)
        {
            // is this item beyond the bottom edge?
//          if (bResultV)
//              bResultV = (itemRef->m_pos.x < m_NativeBoundsR.height);

            if (bResultV)
            {
                boundsR->x = 0;
                boundsR->y = m_defaultItemSize.y * (itemIndex - m_itemViewBaseIndex);
                boundsR->width = m_defaultItemSize.x;
                boundsR->height = m_defaultItemSize.y;

//              if (boundsR->height > m_NativeBoundsR.height - itemRef->m_pos.x)
//                  boundsR->height = m_NativeBoundsR.height - itemRef->m_pos.x;

                bResultV = ((boundsR->width > 0) && (boundsR->height > 0));
            }
        }
        else
        {
            // is this item beyond the right edge?
            if (bResultV)
                bResultV = (itemRef->m_pos.x < m_NativeBoundsR.width);

            if (bResultV)
            {
                boundsR->x = itemRef->m_pos.x;
                boundsR->y = 0; // m_NativeBoundsR.y;
                boundsR->width = itemRef->m_size.x + 1;
                boundsR->height = m_NativeBoundsR.height;

                if (boundsR->width > m_NativeBoundsR.width - itemRef->m_pos.x)
                    boundsR->width = m_NativeBoundsR.width - itemRef->m_pos.x;

                bResultV = ((boundsR->width > 0) && (boundsR->height > 0));
            }
        }
    }

    if (! bResultV)
    {
        boundsR->x =
        boundsR->y =
        boundsR->width =
        boundsR->height = 0;
    }

    return bResultV;
}


wxColumnHeaderItem * wxColumnHeader::GetItemRef( long itemIndex ) const
{
    if ((itemIndex >= 0) && (itemIndex < m_itemCount))
        return m_itemList[itemIndex];
    else
        return NULL;
}


wxBitmap wxColumnHeader::GetLabelBitmap( long itemIndex ) const 
{
    wxColumnHeaderItem      *itemRef;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        return itemRef->GetLabelBitmap( );
    }
    else
    {
        return wxNullBitmap;
    }
}


void wxColumnHeader::SetLabelBitmap( long            itemIndex,
                                     const wxBitmap  &targetBitmap )
{
    wxColumnHeaderItem  *itemRef;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        itemRef->SetLabelBitmap( targetBitmap );
        RefreshItem( itemIndex, true );
    }
}


wxString wxColumnHeader::GetLabelText( long itemIndex ) const
{
    wxColumnHeaderItem      *itemRef;
    wxString                textBuffer;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        textBuffer = itemRef->GetLabelText( );
    }
    return textBuffer;
}


void wxColumnHeader::SetLabelText( long           itemIndex,
                                   const wxString &textBuffer )
{
    wxColumnHeaderItem      *itemRef;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        itemRef->SetLabelText( textBuffer );
        RefreshItem( itemIndex, true );
    }
}


wxAlignment wxColumnHeader::GetLabelAlignment( long itemIndex ) const
{
    wxColumnHeaderItem      *itemRef;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        return itemRef->GetLabelAlignment();
    }

    return (wxAlignment)0;
}


void wxColumnHeader::SetLabelAlignment( long          itemIndex,
                                        wxAlignment   align )
{
    wxColumnHeaderItem      *itemRef;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        itemRef->SetLabelAlignment( align );
        RefreshItem( itemIndex, true );
    }
}


wxPoint wxColumnHeader::GetItemPosition( long itemIndex ) const
{
    wxColumnHeaderItem      *itemRef;
    wxPoint                 pos;
    bool                    bResultV;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);
    if (bResultV)
    {
        pos = itemRef->GetPosition();
    }
    else
    {
        pos.x =
        pos.y = 0;
    }

    return pos;
}


void wxColumnHeader::SetItemPosition( long          itemIndex,
                                      const wxPoint &targetPos )
{
    wxColumnHeaderItem      *itemRef;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        itemRef->SetPosition( targetPos );

        RecalculateItemExtents();
        RefreshItem( itemIndex, true );
    }
}


wxSize wxColumnHeader::GetItemSize( long itemIndex ) const
{
    wxColumnHeaderItem  *itemRef;
    wxSize              resultSize;
    bool                bResultV;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);
    if (bResultV)
    {
        resultSize = itemRef->GetSize();
    }
    else
    {
        resultSize.x =
        resultSize.y = 0;
    }

    return resultSize;
}


void wxColumnHeader::SetItemSize( long          itemIndex,
                                  const wxSize  &targetSize )
{
    wxColumnHeaderItem      *itemRef;

    itemRef = GetItemRef( itemIndex );
    if (itemRef != NULL)
    {
        itemRef->SetSize( targetSize );

        RecalculateItemExtents();
        RefreshItem( itemIndex, true );
    }
}


bool wxColumnHeader::GetItemAttribute( long                        itemIndex,
                                       wxColumnHeaderItemAttribute flagEnum ) const
{
    wxColumnHeaderItem      *itemRef;
    bool                    bResultV;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);
    if (bResultV)
        bResultV = itemRef->GetAttribute( flagEnum );

    return bResultV;
}


bool wxColumnHeader::SetItemAttribute( long                        itemIndex,
                                       wxColumnHeaderItemAttribute flagEnum,
                                       bool                        bFlagValue )
{
    wxColumnHeaderItem      *itemRef;
    bool                    bResultV;

    itemRef = GetItemRef( itemIndex );
    bResultV = (itemRef != NULL);
    if (bResultV)
    {
        if (itemRef->SetAttribute( flagEnum, bFlagValue ))
            RefreshItem( itemIndex, true );
    }

    return bResultV;
}


wxColumnHeaderHitTestResult wxColumnHeader::HitTest( const wxPoint &locationPt )
{
    wxColumnHeaderHitTestResult  resultV;
    bool                         bIsVertical;

    resultV = CH_HITTEST_NoPart;

    bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );
    if (bIsVertical)
    {
        wxRect  boundsR;
        long        i;

        for (i=0; i<m_itemCount; i++)
        {
            if (GetItemBounds( i, &boundsR ))
            {
                if ((locationPt.x >= boundsR.x) && (locationPt.x < boundsR.x + boundsR.width)
                    && (locationPt.y >= boundsR.y) && (locationPt.y < boundsR.y + boundsR.height))
                {
                    resultV = (wxColumnHeaderHitTestResult)i;
                    break;
                }
            }
        }

        return resultV;
    }

    
    long        i;

    for (i=0; i<m_itemCount; i++)
        if (m_itemList[i] != NULL)
            if (m_itemList[i]->HitTest( locationPt ) != 0)
            {
                resultV = (wxColumnHeaderHitTestResult)i;
                break;
            }

    return resultV;
}

//---------------------------------------------------------------------------
// Drawing
//---------------------------------------------------------------------------

long wxColumnHeader::Draw( void )
{
    wxRect      bounds;
    long        i, result = 0;


    // TODO: only paint the items that intersect the update region

    wxPaintDC dc( this );
    dc.DestroyClippingRegion();

    for (i=0; i<m_itemCount; i++)
    {
        if ((i < m_itemViewBaseIndex) || !GetItemVisibility( i ))
            continue;
        
        if (GetItemBounds( i, &bounds ))
        {
            dc.SetClippingRegion( bounds );

#ifdef __WXMAC__
            // if the item is the first or the last then make some adjustments
            // so the left/right border isn't drawn (because it will be
            // clipped.)  See OSAF bug 4250.
            if (i == 0)
            {
                bounds.x -=1;
                bounds.width += 1;
            }
            if (i == m_itemCount-1)
            {
                bounds.width += 1;
            }
#endif
            int w = m_itemList[i]->DrawItem( this, dc, bounds, m_selectionColour,
                                             m_bVisibleSelection, m_bUseGenericRenderer );
            result |= (w == -1);
            dc.DestroyClippingRegion();
        }
    }

    return result;
}


long wxColumnHeader::GetItemLabelWidth(long itemIndex) const
{
    wxBitmap   bmp(1,1);
    wxMemoryDC dc(bmp);
    wxRect     bounds;

    GetItemBounds(itemIndex, &bounds);
    return m_itemList[itemIndex]->DrawItem((wxWindow*)this, dc, bounds, m_selectionColour,
                                           m_bVisibleSelection, m_bUseGenericRenderer );
}


void wxColumnHeader::SetViewDirty( void )
{
    Refresh( false, NULL );
}


void wxColumnHeader::RefreshItem( long itemIndex,
                                  bool bForceRedraw )
{
    if (!ItemInView( itemIndex ))
        return;

    wxRect  wxClientR;
    wxPoint itemOrigin;
    wxSize  itemExtent;

    // NB: may need to set graphics context in some cases!
    // NB: is Freeze-Thaw needed only for wxMac?

#if defined(__WXMAC__)
    if (bForceRedraw)
        Freeze();
#else
    wxUnusedVar(bForceRedraw);
#endif

    wxClientR = GetClientRect();
    itemOrigin = GetItemPosition( itemIndex );
    itemExtent = GetItemSize( itemIndex );
    wxClientR.x = itemOrigin.x;
    wxClientR.width = itemExtent.x;
    Refresh( false, &wxClientR );

#if defined(__WXMAC__)
    if (bForceRedraw)
        Thaw();
#endif
}


void wxColumnHeader::RecalculateItemExtents( void )
{
    long originX, baseOriginX, i;
    bool bIsVertical;

    if (m_itemList != NULL)
    {
        originX = 0;
        baseOriginX = m_itemViewBaseOrigin;
        bIsVertical = GetAttribute( CH_ATTR_VerticalOrientation );

        for (i=0; i<m_itemCount; i++)
            if (m_itemList[i] != NULL)
            {
                m_itemList[i]->m_pos.x = originX - baseOriginX;
                if (! bIsVertical)
                    originX += m_itemList[i]->m_size.x;
            }
    }
}




#if defined(__WXMAC__)
// virtual
void wxColumnHeader::MacControlUserPaneActivateProc( bool bActivating )
{
    // FIXME: is this the right way to handle activate events ???
    Enable( bActivating );
}
#endif


void wxColumnHeader::GenerateSelfEvent( wxEventType eventType )
{
    wxColumnHeaderEvent event( this, eventType );

    (void)GetEventHandler()->ProcessEvent( event );
}



// ----------------------------------------------------------------------------
// wxColumnHeaderEvent
// ----------------------------------------------------------------------------

wxColumnHeaderEvent::wxColumnHeaderEvent(
    wxColumnHeader  *col,
    wxEventType     type )
    :
    wxCommandEvent( type, col->GetId() )
{
    SetEventObject( col );
}


void wxColumnHeaderEvent::Init( void )
{
}


// ----------------------------------------------------------------------------
// wxColumnHeaderItem
// ----------------------------------------------------------------------------

wxColumnHeaderItem::wxColumnHeaderItem()
    :
    m_alignment( wxALIGN_LEFT )
    , m_pos( 0, 0 )
    , m_size( 0, 0 )
    , m_visible( true )
    , m_enabled( false )
    , m_selected( false )
    , m_sortEnabled( false )
    , m_sortAscending( false )
    , m_fixedWidth( false )
    , m_isHot( false )
{
}

wxColumnHeaderItem::wxColumnHeaderItem( const wxColumnHeaderItem  *info )
    :
    m_alignment( wxALIGN_LEFT )
    , m_pos( 0, 0 )
    , m_size( 0, 0 )
    , m_visible( true )
    , m_enabled( false )
    , m_selected( false )
    , m_sortEnabled( false )
    , m_sortAscending( false )
    , m_fixedWidth( false )
    , m_isHot( false )
{
    SetItemData( info );
}

wxColumnHeaderItem::~wxColumnHeaderItem()
{
}


// NB: a copy and nothing else...
//
void wxColumnHeaderItem::GetItemData( wxColumnHeaderItem *info ) const
{
    if (info == NULL)
        return;

    info->m_alignment = m_alignment;
    info->m_pos = m_pos;
    info->m_size = m_size;
    info->m_visible = m_visible;
    info->m_enabled = m_enabled;
    info->m_selected = m_selected;
    info->m_sortEnabled = m_sortEnabled;
    info->m_sortAscending = m_sortAscending;
    info->m_fixedWidth = m_fixedWidth;
    info->m_isHot = m_isHot;
    info->m_labelText = m_labelText;
    info->m_bitmap = m_bitmap;
}


void wxColumnHeaderItem::SetItemData( const wxColumnHeaderItem *info )
{
    if (info == NULL)
        return;

    m_alignment = info->m_alignment;
    m_pos = info->m_pos;
    m_size = info->m_size;
    m_visible = info->m_visible;
    m_enabled = info->m_enabled;
    m_selected = info->m_selected;
    m_sortEnabled = info->m_sortEnabled;
    m_sortAscending = info->m_sortAscending;
    m_fixedWidth = info->m_fixedWidth;
    m_isHot = info->m_isHot;
    m_labelText = info->m_labelText;
    m_bitmap = info->m_bitmap;
}


wxBitmap wxColumnHeaderItem::GetLabelBitmap() const
{
    return m_bitmap;
}


void wxColumnHeaderItem::SetLabelBitmap( const wxBitmap  &targetBitmap)
{
    m_bitmap = targetBitmap;
}


wxString wxColumnHeaderItem::GetLabelText( ) const
{
    return m_labelText;
}

void wxColumnHeaderItem::SetLabelText( const wxString &textBuffer )
{
    m_labelText = textBuffer;
}


wxAlignment wxColumnHeaderItem::GetLabelAlignment( void ) const
{
    return m_alignment;
}


void wxColumnHeaderItem::SetLabelAlignment( wxAlignment align )
{
    m_alignment = align;
}


wxPoint wxColumnHeaderItem::GetPosition( void ) const
{
    return m_pos;
}


void wxColumnHeaderItem::SetPosition( const wxPoint &targetPos )
{
    wxUnusedVar( targetPos );

    // NB: not currently permitted
//  if ((targetPos.x >= 0) && (m_pos.x != targetPos.x))
//      m_pos.x = targetPos.x;
}


wxSize wxColumnHeaderItem::GetSize( void ) const
{
    return m_size;
}


void wxColumnHeaderItem::SetSize( const wxSize &targetSize )
{
    m_size = targetSize;
    if (m_size.x < 0)
        m_size.x = 0;
    if (m_size.y < 0)
        m_size.y = 0;
}


void wxColumnHeaderItem::ResizeToWidth( long extentX )
{
    if ((extentX >= 0) && (m_size.x != extentX))
    {
        m_size.x = extentX;
    }
}


bool wxColumnHeaderItem::GetAttribute( wxColumnHeaderItemAttribute flagEnum ) const
{
    bool            bResult;

    bResult = false;

    switch (flagEnum)
    {
        case CH_ITEM_ATTR_Enabled:
            bResult = m_enabled;
            break;
        
        case CH_ITEM_ATTR_Selected:
            bResult = m_selected;
            break;
        
        case CH_ITEM_ATTR_SortEnabled:
            bResult = m_sortEnabled;
            break;
        
        case CH_ITEM_ATTR_SortDirection:
            bResult = m_sortAscending;
            break;
        
        case CH_ITEM_ATTR_FixedWidth:
            bResult = m_fixedWidth;
            break;
        
        default:
            break;
    }

    return bResult;
}


bool wxColumnHeaderItem::SetAttribute( wxColumnHeaderItemAttribute flagEnum,
                                       bool                        bFlagValue )
{
    bool            bResult;

    bResult = true;

    switch (flagEnum)
    {
        case CH_ITEM_ATTR_Enabled:
            m_enabled = bFlagValue;
            break;
        
        case CH_ITEM_ATTR_Selected:
            m_selected = bFlagValue;
            break;
        
        case CH_ITEM_ATTR_SortEnabled:
            m_sortEnabled = bFlagValue;
            break;
        
        case CH_ITEM_ATTR_SortDirection:
            m_sortAscending = bFlagValue;
            break;
        
        case CH_ITEM_ATTR_FixedWidth:
            m_fixedWidth = bFlagValue;
            break;
        
        default:
            bResult = false;
            break;
    }

    return bResult;
}


long wxColumnHeaderItem::HitTest( const wxPoint &locationPt ) const
{
    long        targetX, resultV;

    targetX = locationPt.x;
    resultV = ((targetX >= m_pos.x) && (targetX < m_pos.x + m_size.x));

    return resultV;
}


long wxColumnHeaderItem::DrawItem( wxWindow*       parent,
                                   wxDC&           dc,
                                   const wxRect&   bounds,
                                   const wxColour& selectionColour,
                                   bool            visibleSelection,
                                   bool            useGeneric )
{
    if ( bounds.IsEmpty() || !parent || !dc.Ok() )
        return -1;

    wxHeaderButtonParams params;
    int flags = 0;
    wxHeaderSortIconType arrow = wxHDR_SORT_ICON_NONE;
    
    if ( m_selected && visibleSelection )
        flags |= wxCONTROL_SELECTED;

    if ( m_isHot )
        flags |= wxCONTROL_CURRENT;
        
    if ( m_selected && m_sortEnabled && m_sortAscending )
        arrow = wxHDR_SORT_ICON_UP;

    if ( m_selected && m_sortEnabled && !m_sortAscending )
        arrow = wxHDR_SORT_ICON_DOWN;
        
    params.m_labelText = m_labelText;
    params.m_labelFont = parent->GetFont();
    params.m_selectionColour = selectionColour;
    params.m_labelBitmap = m_bitmap;
        
    params.m_labelAlignment = m_alignment;

    // Render the header item
    if (useGeneric)
        return wxRendererNative::GetGeneric().DrawHeaderButton(parent, dc, bounds,
                                                        flags, arrow, &params);
    else        
        return wxRendererNative::Get().DrawHeaderButton(parent, dc, bounds,
                                                 flags, arrow, &params);
}


// #endif // wxUSE_COLUMNHEADER


