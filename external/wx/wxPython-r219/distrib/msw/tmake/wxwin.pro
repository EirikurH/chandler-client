###############################################################################
# File:    wxwin.pro
# Purpose: tmake project file from which makefiles for wxWindows are generated
#          tmake -t vc wxwin.pro
# Author:  Vadim Zeitlin
# Created: 14.07.99
# Version: $Id: wxwin.pro 3013 1999-07-15 22:46:31Z VZ $
###############################################################################

# this file is empty
