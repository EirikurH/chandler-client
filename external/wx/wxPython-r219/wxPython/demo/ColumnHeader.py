#
# ColumnHeader.py
#

import wx
import wx.colheader
import images

#----------------------------------------------------------------------

class TestPanel( wx.Panel ):
    def __init__( self, parent, log ):
        wx.Panel.__init__( self, parent, -1, style=wx.NO_FULL_REPAINT_ON_RESIZE )
        self.log = log

        # init (non-UI) demo vars
        # NB: should be 17 for Mac; 20 for all other platforms
        # wxColumnHeader can handle it
        self.colHeight = wx.colheader.ColumnHeader.GetFixedHeight()
        self.baseWidth1 = 350
        self.baseWidth2 = 270
        self.colStartX = 175
        self.colStartY = 20

        resizeColStartX = self.colStartX

        self.stepSize = 0
        self.stepDir = -1

        # "no" to sort arrows for this list
        self.baseCntlID = 1001
        prompt = "ColumnHeader (%d)" %(self.baseCntlID)
        l1 = wx.StaticText( self, -1, prompt, (self.colStartX, self.colStartY), (200, 20) )

        ch1 = wx.colheader.ColumnHeader( self, self.baseCntlID, (self.colStartX, self.colStartY + 20), (self.baseWidth1, self.colHeight), 0 )
        dow = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ]
        for v in dow:
            ch1.AddItem( -1, v, wx.ALIGN_CENTER, 50, 0, 0, 1 )
        ch1.SetSelectedItem( 0 )

        self.ch1 = ch1
        self.Bind( wx.colheader.EVT_COLUMNHEADER_SELCHANGED, self.OnClickColumnHeader, ch1 )
        #ch1.SetToolTipString( "ColumnHeader (%d)" %(self.baseCntlID) )

        # "yes" to sort arrows for this list
        prompt = "ColumnHeader (%d)" %(self.baseCntlID + 1)
        l2 = wx.StaticText( self, -1, prompt, (self.colStartX, self.colStartY + 80), (200, 20) )

        ch2 = wx.colheader.ColumnHeader( self, self.baseCntlID + 1, (self.colStartX, self.colStartY + 100), (self.baseWidth2, self.colHeight), 0 )
        coffeeNames = [ "Juan", "Valdez", "the coffee guy" ]
        textAligns = [ wx.ALIGN_LEFT, wx.ALIGN_CENTER, wx.ALIGN_RIGHT ]
        for i, v in enumerate( coffeeNames ):
            ch2.AddItem( -1, v, textAligns[i], 90, 0, 1, 1 )
        ch2.SetSelectedItem( 0 )

        self.ch2 = ch2
        self.Bind( wx.colheader.EVT_COLUMNHEADER_SELCHANGED, self.OnClickColumnHeader, ch2 )
        #ch2.SetToolTipString( "ColumnHeader (%d)" %(cntlID) )

        # row header
        # "no" to sort arrows for this list
        prompt = "RowHeader (%d)" %(self.baseCntlID + 2)
        l3 = wx.StaticText( self, -1, prompt, (self.colStartX + 400, self.colStartY), (200, 20) )

        ch3 = wx.colheader.ColumnHeader( self, self.baseCntlID + 2, (self.colStartX + 400, self.colStartY + 20), (100, 5 * self.colHeight), wx.colheader.CH_STYLE_HeaderIsVertical )
        fancifulRowNames = [ "Stacked", "Tower", "Big", "Babylon", "Story" ]
        for i, v in enumerate( fancifulRowNames ):
            ch3.AddItem( -1, v, wx.ALIGN_CENTER, -1, 0, 0, 1 )
        ch3.ResizeToFit()
        ch3.SetSelectedItem( 2 )

        self.ch3 = ch3
        self.Bind( wx.colheader.EVT_COLUMNHEADER_SELCHANGED, self.OnClickColumnHeader, ch3 )
        #ch3.SetToolTipString( "RowHeader (%d)" %(cntlID) )

        # add demo UI controls
        self.colStartX = 10
        miscControlsY = 190

        cb1 = wx.CheckBox( self, -1, "Enable", (self.colStartX, miscControlsY), (100, 20), wx.NO_BORDER )
        self.Bind( wx.EVT_CHECKBOX, self.OnTestEnableCheckBox, cb1 )
        cb1.SetValue( ch1.IsEnabled() )

        # NB: generic rendering is mandatory for non-[Mac,Win] platforms
        cb2 = wx.CheckBox( self, -1, "Generic Renderer", (self.colStartX, miscControlsY + 25), (150, 20), wx.NO_BORDER )
        self.Bind( wx.EVT_CHECKBOX, self.OnTestGenericRendererCheckBox, cb2 )

        cb3 = wx.CheckBox( self, -1, "Visible Selection", (self.colStartX, miscControlsY + 50), (150, 20), wx.NO_BORDER )
        self.Bind( wx.EVT_CHECKBOX, self.OnTestVisibleSelectionCheckBox, cb3 )
        cb3.SetValue( ch1.GetAttribute( wx.colheader.CH_ATTR_VisibleSelection ) )

        cb4 = wx.CheckBox( self, -1, "Proportional Resizing", (self.colStartX, miscControlsY + 75), (165, 20), wx.NO_BORDER )
        self.Bind( wx.EVT_CHECKBOX, self.OnTestProportionalResizingCheckBox, cb4 )
        cb4.SetValue( ch1.GetAttribute( wx.colheader.CH_ATTR_ProportionalResizing ) )

        prompt = "Default fixed item size: [%ld]" %(wx.colheader.ColumnHeader.GetFixedHeight())
        lXX = wx.StaticText( self, -1, prompt, (self.colStartX, miscControlsY + 175), (200, 20) )

        l0O = wx.StaticText( self, -1, "Last action:", (self.colStartX, miscControlsY + 200), (90, 20) )
        l0 = wx.StaticText( self, -1, "[result]", (self.colStartX + 95, miscControlsY + 200), (250, 20) )
        self.l0 = l0

        btn = wx.Button( self, -1, "Delete Selection", (10, self.colStartY + 15) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestDeleteItem, btn )

        btn = wx.Button( self, -1, "Add Bitmap Item", (10, self.colStartY + 80 + 5) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestAddBitmapItem, btn )

        btn = wx.Button( self, -1, "Resize Division", (10, self.colStartY + 80 + 5 + 30) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestResizeDivision, btn )

        self.colStartX += 165


        self.colStartX += 175

        btn = wx.Button( self, -1, "Deselect", (self.colStartX, miscControlsY) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestDeselect, btn )

        btn = wx.Button( self, -1, "Show/Hide Selection", (self.colStartX, miscControlsY + 30) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestShowHide, btn )

        btn = wx.Button( self, -1, "Scroll", (self.colStartX, miscControlsY + 60) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestScroll, btn )

        btn = wx.Button( self, -1, "Resize Bounds", (self.colStartX, miscControlsY + 90) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestResizeBounds, btn )

        btn = wx.Button( self, -1, "Resize To Fit", (self.colStartX, miscControlsY + 120) )
        self.Bind( wx.EVT_BUTTON, self.OnButtonTestResizeToFit, btn )

        self.colStartX = resizeColStartX

    def OnClickColumnHeader( self, event ):
        ch = event.GetEventObject()
        self.l0.SetLabel( "(%d): clicked - selected (%ld)" %(event.GetId(), ch.GetSelectedItem()) )
        # self.log.write( "Click! (%ld)\n" % event.GetEventType() )

    def OnButtonTestDeselect( self, event ):
        self.ch1.SetSelectedItem( -1 )
        self.ch2.SetSelectedItem( -1 )
        self.ch3.SetSelectedItem( -1 )
        self.l0.SetLabel( "(all): deselected items" )

    def OnButtonTestShowHide( self, event ):
        curSelected = self.ch1.GetSelectedItem()
        if (curSelected < 0):
            isVisible = 1
        else:
            isVisible = 0
        if (isVisible):
            # @@@ change to show all items
            self.ch1.SetItemVisibility( curSelected, isVisible )
            self.ch2.SetItemVisibility( curSelected, isVisible )
            self.ch3.SetItemVisibility( curSelected, isVisible )
            self.l0.SetLabel( "(all): show (all) hidden item(s)" )
        else:
            self.ch1.SetItemVisibility( curSelected, isVisible )
            self.ch2.SetItemVisibility( curSelected, isVisible )
            self.ch3.SetItemVisibility( curSelected, isVisible )
            self.l0.SetLabel( "(all): hide selected items" )

    def OnButtonTestScroll( self, event ):
        curBase = self.ch1.GetBaseViewItem()
        if (curBase == 0):
            curBase = 1
        else:
            curBase = 0
        self.ch1.SetBaseViewItem( curBase )
        self.ch2.SetBaseViewItem( curBase )
        self.ch3.SetBaseViewItem( curBase )
        self.l0.SetLabel( "(all): scrolled items" )

    def OnButtonTestResizeBounds( self, event ):
        if (self.stepSize == 1):
            self.stepDir = (-1)
        else:
            if (self.stepSize == (-1)):
                self.stepDir = 1
        self.stepSize = self.stepSize + self.stepDir
        ch = self.ch1
        newSize = self.baseWidth1 + 40 * self.stepSize
        ch.DoSetSize( self.colStartX, self.colStartY + 20, newSize, 20, 0 )
        ch = self.ch2
        newSize = self.baseWidth2 + 40 * self.stepSize
        ch.DoSetSize( self.colStartX, self.colStartY + 100, newSize, 20, 0 )
        self.l0.SetLabel( "(both): resized bounds by (%d)" %(40 * self.stepSize) )

    def OnButtonTestResizeToFit( self, event ):
        self.ch1.ResizeToFit()
        self.ch2.ResizeToFit()
        self.ch3.ResizeToFit()
        self.l0.SetLabel( "(both): resized and refitted!" )

    def OnButtonTestDeleteItem( self, event ):
        ch = self.ch1
        itemIndex = ch.GetSelectedItem()
        if (itemIndex >= 0):
            ch.DeleteItem( itemIndex )
            self.baseWidth1 -= 70
            self.l0.SetLabel( "(%d): deleted item (%d)" %(ch.GetId(), itemIndex) )
        else:
            self.l0.SetLabel( "(%d): no item selected" %(ch.GetId()) )

    def OnButtonTestAddBitmapItem( self, event ):
        ch = self.ch2
        itemCount = ch.GetItemCount()
        if (itemCount <= 8):
             itemIndex = ch.GetSelectedItem()
             if (itemIndex < 0):
                 itemIndex = itemCount
             ch.AddItem( itemIndex, "", wx.ALIGN_CENTER, 40, 0, 0, 1 )
             ch.SetItemAttribute( itemIndex, wx.colheader.CH_ITEM_ATTR_FixedWidth, 1 )
             testBmp = images.getTest2Bitmap()
             testBmp.SetMaskColour((0, 0, 0xFF))
             #testBmp = wx.Bitmap(r"C:\PROJECTS\osaf\wx-trunk\tmp\SumMailPressed.png")
             ch.SetLabelBitmap( itemIndex, testBmp )
             ch.SetSelectedItem( itemIndex )
             ch.ResizeToFit()
             self.baseWidth2 += 40
             self.l0.SetLabel( "(%d): added bitmap item (%d)" %(ch.GetId(), itemIndex) )
        else:
             self.l0.SetLabel( "(%d): enough items!" %(ch.GetId()) )

    def OnButtonTestResizeDivision( self, event ):
        ch = self.ch2
        itemIndex = ch.GetSelectedItem()
        if ((itemIndex > 0) and (itemIndex < ch.GetItemCount())):
            curExtent = ch.GetUIExtent( itemIndex )
            ch.ResizeDivision( itemIndex, curExtent.x - 5 )
            self.l0.SetLabel( "(%d): resized btw. %d and %d" %(ch.GetId(), itemIndex - 1, itemIndex) )
        else:
            self.l0.SetLabel( "(%d): no valid item selected" %(ch.GetId()) )

    def OnTestEnableCheckBox( self, event ):
        curEnabled = self.ch1.IsEnabled()
        curEnabled = not curEnabled
        self.ch1.Enable( curEnabled )
        self.ch2.Enable( curEnabled )
        self.ch3.Enable( curEnabled )
        self.l0.SetLabel( "enabled (%d)" %(curEnabled) )

    def OnTestGenericRendererCheckBox( self, event ):
        curEnabled = self.ch1.GetAttribute( wx.colheader.CH_ATTR_GenericRenderer )
        curEnabled = not curEnabled
        self.ch1.SetAttribute( wx.colheader.CH_ATTR_GenericRenderer, curEnabled )
        self.ch2.SetAttribute( wx.colheader.CH_ATTR_GenericRenderer, curEnabled )
        self.ch3.SetAttribute( wx.colheader.CH_ATTR_GenericRenderer, curEnabled )
        self.l0.SetLabel( "generic renderer (%d)" %(curEnabled) )

    def OnTestVisibleSelectionCheckBox( self, event ):
        curEnabled = self.ch1.GetAttribute( wx.colheader.CH_ATTR_VisibleSelection )
        curEnabled = not curEnabled
        self.ch1.SetAttribute( wx.colheader.CH_ATTR_VisibleSelection, curEnabled )
        self.ch2.SetAttribute( wx.colheader.CH_ATTR_VisibleSelection, curEnabled )
        self.ch3.SetAttribute( wx.colheader.CH_ATTR_VisibleSelection, curEnabled )
        self.l0.SetLabel( "selection visible (%d)" %(curEnabled) )

    def OnTestProportionalResizingCheckBox( self, event ):
        curEnabled = self.ch1.GetAttribute( wx.colheader.CH_ATTR_ProportionalResizing )
        curEnabled = not curEnabled
        self.ch1.SetAttribute( wx.colheader.CH_ATTR_ProportionalResizing, curEnabled )
        self.ch2.SetAttribute( wx.colheader.CH_ATTR_ProportionalResizing, curEnabled )
        self.ch3.SetAttribute( wx.colheader.CH_ATTR_ProportionalResizing, curEnabled )
        self.l0.SetLabel( "proportional resizing (%d)" %(curEnabled) )


#----------------------------------------------------------------------

def runTest( frame, nb, log ):
    win = TestPanel( nb, log )
    return win

#----------------------------------------------------------------------


overview = """<html><body>
<h2>ColumnHeader</h2>

<p>A ColumnHeader control displays a set of joined, native-appearance button-ish things.</p>

<p>RowHeaders (vertical orientation) are also supported.</p>

<p>Native column (and row) headers can be found in many views, most notably in a folder Details view.</p>

<p>This control embodies the native look and feel to the greatest practical degree, and fills in some holes to boot.</p>

<p>Selections, bitmaps, button arrows and sort arrows are optional.</p>

<p>NB-1: a limitation - text and bitmaps are mutually exclusive.</p>

<p>NB-2: not all of the selection styles are implemented.</p>

<p>NB-3: this class has been integrated into wxGrid by means of a shim subclass and 8 tweaks to wxGrid C++.</p>

<p>The MSW version of this control has a persistent selection indicator. The native MSW control has no canonical selection UI, instead using a sort arrow to serve double-duty as a selection indicator; nonetheless, it has a rollover indicator.</p>

<p>The GTK framework lacks, or appears to lack, a native control: a simple bevel button shall suffice for the theme background.</p>

</body></html>
"""


if __name__ == '__main__':
    import sys,os
    import run
    run.main(['', os.path.basename(sys.argv[0])] + sys.argv[1:])

