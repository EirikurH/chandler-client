/////////////////////////////////////////////////////////////////////////////
// Name:        wxPython/src/colheader.i
// Purpose:    SWIG definitions for the wxColumnHeader wxWidget
//
// Author:      David Surovell
//
/////////////////////////////////////////////////////////////////////////////

%define DOCSTRING
"Classes for a column header control with a native appearance."
%enddef

%module(package="wx", docstring=DOCSTRING) colheader


%{
#include "wx/wxPython/wxPython.h" 
#include "wx/wxPython/pyclasses.h"

#include <wx/colheader.h>
%}

//----------------------------------------------------------------------

%import misc.i
%pythoncode { wx = _core }
%pythoncode { __docfilter__ = wx.__DocFilter(globals()) }

//---------------------------------------------------------------------------

enum wxColumnHeaderHitTestResult
{
    CH_HITTEST_NoPart          = -1,    // not within a known sub-item (but within the client bounds)
    CH_HITTEST_ItemZero        = 0      // any other (non-negative) value is a sub-item
};

enum wxColumnHeaderStyle
{
    CH_STYLE_HeaderIsVertical
};

enum wxColumnHeaderAttribute
{
    CH_ATTR_VerticalOrientation,
    CH_ATTR_GenericRenderer,
    CH_ATTR_FixedHeight,
    CH_ATTR_ProportionalResizing,
    CH_ATTR_VisibleSelection,
};

enum wxColumnHeaderItemAttribute
{
    CH_ITEM_ATTR_Enabled,
    CH_ITEM_ATTR_Selected,
    CH_ITEM_ATTR_SortEnabled,
    CH_ITEM_ATTR_SortDirection,
    CH_ITEM_ATTR_FixedWidth
};

//---------------------------------------------------------------------------

class wxColumnHeader;

class wxColumnHeaderEvent : public wxCommandEvent
{
public:
    wxColumnHeaderEvent( wxColumnHeader *col, wxEventType type );
};


%constant wxEventType wxEVT_COLUMNHEADER_DOUBLECLICKED;
%constant wxEventType wxEVT_COLUMNHEADER_SELCHANGED;


%pythoncode {
EVT_COLUMNHEADER_DOUBLECLICKED = wx.PyEventBinder(wxEVT_COLUMNHEADER_DOUBLECLICKED, 1)
EVT_COLUMNHEADER_SELCHANGED = wx.PyEventBinder(wxEVT_COLUMNHEADER_SELCHANGED, 1)
}


//---------------------------------------------------------------------------
MustHaveApp(wxColumnHeader);

class wxColumnHeader : public wxControl
{
public:
    %pythonAppend wxColumnHeader      "self._setOORInfo(self)"
    %pythonAppend wxColumnHeader()    ""

    wxColumnHeader(
        wxWindow        *parent,
        wxWindowID        id = wxID_ANY,
        const wxPoint        &pos = wxDefaultPosition,
        const wxSize        &size = wxDefaultSize,
        long                styleVariant = 0,
        const wxString        &name = wxColumnHeaderNameStr );

    %RenameCtor(PreColumnHeader, wxColumnHeader());

 
    wxSize CalculateDefaultSize( void ) const;
    wxSize GetDefaultItemSize( void ) const;
    void SetDefaultItemSize( const wxSize& targetSize );

    wxSize CalculateDefaultItemSize( const wxSize &maxSize );

    wxSize GetTotalUIExtent( long itemCount = (-1),
                             bool  bStartAtBase = false ) const;
    bool ResizeToFit( long itemCount = (-1) );
    bool RescaleToFit( long newWidth );
    bool ResizeDivision( long itemIndex,
                         long originX );

    wxColour GetSelectionColour() const;
    void SetSelectionColour( const wxColour &targetColour );

    bool GetAttribute( wxColumnHeaderAttribute flagEnum ) const;
    bool SetAttribute( wxColumnHeaderAttribute flagEnum,
                       bool                    bFlagValue );

    wxColumnHeaderHitTestResult HitTest( const wxPoint &locationPt );
    long GetSelectedItem( void ) const;
    void SetSelectedItem( long itemIndex );
    long GetBaseViewItem( void ) const;
    void SetBaseViewItem( long itemIndex );

    long GetItemCount( void ) const;
    void SetItemCount( long itemCount );

    void DeleteItems( long itemIndex,
                      long itemCount );
    void DeleteItem(  long itemIndex );
    
    void AppendItem( const wxString  &textBuffer,
                     wxAlignment     align,
                     long            width = (-1),
                     bool            bSelected = false,
                     bool            bSortEnabled = false,
                     bool            bSortAscending = false );
    void AddEmptyItems( long beforeIndex,
                        long itemCount );
    void AddItem( long           beforeIndex,
                  const wxString &textBuffer,
                  wxAlignment    align,
                  long           width = (-1),
                  bool           bSelected = false,
                  bool           bSortEnabled = false,
                  bool           bSortAscending = false );

    bool GetItemVisibility( long itemIndex ) const;
    void SetItemVisibility( long itemIndex,
                            bool bVisible );

    wxBitmap GetLabelBitmap( long itemIndex ) const ;
    void SetLabelBitmap( long           itemIndex,
                         const wxBitmap &imageRef );

    wxString GetLabelText( long itemIndex ) const;
    void SetLabelText( long           itemIndex,
                       const wxString &textBuffer );

    wxAlignment GetLabelAlignment( long itemIndex ) const;
    void SetLabelAlignment( long         itemIndex,
                            wxAlignment  align );

    wxPoint GetItemPosition( long itemIndex ) const;
    void SetItemPosition( long itemIndex, const wxPoint &targetPos );

    wxSize GetItemSize( long itemIndex ) const;
    void SetItemSize(long itemIndex, const wxSize &targetSize );

    bool GetItemAttribute( long                        itemIndex,
                           wxColumnHeaderItemAttribute flagEnum ) const;
    bool SetItemAttribute( long                        itemIndex,
                           wxColumnHeaderItemAttribute flagEnum,
                           bool                        bFlagValue );
    long GetItemLabelWidth(long itemIndex) const;

    static long GetFixedHeight( void );

    static void GetDefaultLabelValue( bool      isColumn,
                                      long      itemIndex,
                                      wxString& value );
};

//---------------------------------------------------------------------------

%init %{
%}

//---------------------------------------------------------------------------

