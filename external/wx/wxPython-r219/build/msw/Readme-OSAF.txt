
When new project or workspace files are merged in from upstream wx, or
when bakefile is run to regenerate these files, do the following steps
to make a new copy of the wx_osaf solution and project files.  We have
to do this because Bakefile only generates the MSVC 6 project files,
so we have to import and save those to make a MSVC 7.1 version of the
project files.

1. Copy the generated wx.dsw to wx_osaf.dsw:

   	cp wx.dsw wx_osaf.dsw

2. Run the MS DevEnv and open wx_osaf.dsw (*not* the wx_osaf.sln, we
   will be saving over that with the new one.)  Answer yes to the
   dialog asking if you want to migrate to the new format.

3. Add the following project files to the solution:

        wx/contrib/build/gizmos/gizmos_gizmos.dsp
        wx/contrib/build/stc/stc.dsp

4. Remove the following projects from the solution:

   	dbgrid
	odbc

5. Open the solution's project dependencies dialog and specify the
   following dependencies:

       adv      --> core, html, xml
       aui      --> adv
       base     --> wxexpat, wxjpeg, wxpng, wxregex, wxzlib
       core     --> base
       gizmos   --> adv
       gl       --> core
       html     --> core, net
       media    --> core
       net      --> base
       qa       --> core, xml
       richtext --> adv
       stc      --> core
       xrc      --> adv, core, html, xml
    	
6. Save the solution and exit.  You can now build using the OSAF Makefile.
