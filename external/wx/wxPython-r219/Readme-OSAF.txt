How to merge wx from upstream sources
=====================================

This SVN repository is for tracking the wxWidgets source repository
and OSAF's customizations of wxWidgets.  To facilitate the merging of
upstream sources into OSAF's modified version of the code we've
decided on a repository structure containing the trunk and a single
branch (besides any that may be created for normal development
purposes.)  

The wx-upstream branch is basically just a mirror of of a specific
revision pulled from the wxWidgets SVN repository.  There is no effort
made to keep all revisions from that repository synced or to track
more than one branch.  It is simply a snapshot, and previous revisions
in this branch are previous snapshots.  The only checkins that should
be done in the wx-upstream branch are for updates from the wxWidgets
server, (making a new snapshot if you will) the steps for doing this
are described below.  There should *never* be any checkins to the
wxWidgets SVN server done from here, feeding changes back upstream
will have to follow a different path.

The trunk in this repository is where the OSAF customized version of
the wxWidgets source tree resides.  This tree is to be used for
development and testing of wxWidgets for OSAF, and is where snapshots
of the code are pulled from for the wx tarballs used in Chandler.

Following are the steps needed to do an update from the upstream
sources into OSAF's version of the wxWidgets sources.  We basically
just need to do an export of a spefic revision, tag or branch from the
wx SVN server, and then commit those files to the OSAF SVN for wx.


1. Cd to the location where you have the wx-upstream branch checked
   out.

    cd wx-upstream

2. Delete all exising files except for the .svn dirs and their
   contents.  This is done so it is easy to see what files, if any,
   should be deleted by this snapshot's commit, because they will be
   missing after the export is done.

     find . -type f | grep -v /.svn | xargs rm

3. Depending on if a tagged version is being exported, or if a
   revision from the trunk or a branch, then there will either be one
   export required, or two exports with one going into a subdir of the
   other.  This is because wxWidgets and wxPython are now in different
   locations of the repository, but when a release is done I make a
   combined tree for the tagged version.  So the export command(s)
   will either look like this:

    svn export --force https://svn.wxwidgets.org/svn/wx/wxPython/tags/wxPy-2.8.6.0 .

   or like this:

    svn export --force https://svn.wxwidgets.org/svn/wx/wxWidgets/branches/WX_2_8_BRANCH .
    svn export --force https://svn.wxwidgets.org/svn/wx/wxPython/branches/WX_2_8_BRANCH wxPython

4. Use a SVN GUI tool or the svn status command to find any files that
   were formerly in the OSAF SVN but are now missing, and remove them.

5. Add any new files that were previously not in the OSAF SVN but are
   now in your workspace and add them.  Make sure to watch for added
   or removed subdirs too.

6. Commit all changes.


The next step after all that is done is to merge the changes made on
the branch to the trunk of the OSAF wx repository.


7. In your wx trunk workspace make sure that it is up to date and that
   there are no pending modifications that need checked in.

8. Run a command similar to the following to do the merge.  The first
   rev number is the revision of the last merge point in wx-upstream,
   and the second is the current HEAD on wx-upstream.  NOTE: Look in
   the .last-merge file for these details, or check the subversion
   log.

     svn merge -r 175:188 svn+ssh://svn.osafoundation.org/svn/wx/branches/wx-upstream

9. Run bakefile if wx added or removed source files.  (Some of these
   may result in zero changes if the merge process was able to handle
   the changes.)

     cd build/bakefiles
     bakefile_gen -b wx.bkl
     bakefile_gen -b ../../contrib/build/gizmos/gizmos.bkl
     bakefile_gen -b ../../contrib/build/stc/stc.bkl

10. Resolve merge conflicts.

11. Do test builds.  NOTE: I use yet another source code control
    system to help me copy files between my local machines.  This
    enables me to run tests of the new code on all machines without
    requiring to first check in changes to the SVN repository and then
    have to recommit fixes later, or to do something messy like
    manually coping files back and forth which is prone to forgetting
    something along the way.

    I've chosen Mecurial for this purpose for two reasons, first it is
    written in Python <wink!> And secondly because it leaves very few
    tracks in the file system that have to be ignored by subversion,
    just a single dir and one file at the top of the file tree.

    http://www.selenic.com/mercurial/wiki/index.cgi/Mercurial

12. If anything was updated by the bakefile step, or if new project
    files have been added, then the MSVC project files need updated
    too.  You need to re-import the MSVC 6 workspace and projects, set
    the build dependencies, and etc. as described in the
    Readme-OSAF.txt file located in build/msw.

13. If the version number of wxPython changed then be sure to update
    it in the Makefile.

13. Check-in the merged source tree.  In the log message be sure to
    mention the range of revisions used in the merge command as this
    is a common practice mentioned in the Subversion book.  Also
    update the .last-merge file in the root of the tree.  (I've
    started putting the same text here as what I use for the merge
    commit message.  That makes it easier to remember what to write
    next time.) 

14. When ready to roll the new version over to Chandler then update
    the wxPython version and the SVNVER value in external/wx/Makefile
    and commit the change.  The Tinderbox processes will then pull
    that SVNVER revision for their next build.  

15. When that is done and the new tarballs have been moved to the
    download area, then edit chandler/Makefile to reference the new
    tarball version, and test doing an update and install.  Finally
    check-in chandler/Makefile for other developers to be able to
    start using the new wx tarball.
