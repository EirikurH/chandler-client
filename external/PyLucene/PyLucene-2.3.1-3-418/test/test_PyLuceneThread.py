# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

import time, threading
from unittest import TestCase, main
from lucene import *


class PyLuceneThreadTestCase(TestCase):
    """
    Test using threads in PyLucene with python threads
    """

    def setUp(self):

        self.classLoader = Thread.currentThread().getContextClassLoader()

        self.directory = RAMDirectory()
        writer = IndexWriter(self.directory, StandardAnalyzer(), True)

        doc1 = Document()
        doc2 = Document()
        doc3 = Document()
        doc4 = Document()
        doc1.add(Field("field", "one",
                       Field.Store.YES, Field.Index.TOKENIZED))
        doc2.add(Field("field", "two",
                       Field.Store.YES, Field.Index.TOKENIZED))
        doc3.add(Field("field", "three",
                       Field.Store.YES, Field.Index.TOKENIZED))
        doc4.add(Field("field", "one",
                       Field.Store.YES, Field.Index.TOKENIZED))

        writer.addDocument(doc1)
        writer.addDocument(doc2)
        writer.addDocument(doc3)
        writer.addDocument(doc4)
        writer.optimize()
        writer.close()

        self.testData = [('one',2), ('two',1), ('three', 1), ('five', 0)] * 500
        self.lock = threading.Lock()
        self.totalQueries = 0


    def tearDown(self):

        self.directory.close()


    def testWithMainThread(self):
        """ warm up test for runSearch in main thread """

        self.runSearch(2000, True)


    def testWithPyLuceneThread(self):
        """ Run 5 threads with 2000 queries each """

        threads = []
        for i in xrange(5):
            threads.append(threading.Thread(target=self.runSearch,
                                            args=(2000,)))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        # we survived!

        # and all queries have ran successfully
        self.assertEqual(10000, self.totalQueries)


    def runSearch(self, runCount, mainThread=False):
        """ search for runCount number of times """

        # problem: if there are any assertion errors in the child
        #   thread, the calling thread is not notified and may still
        #   consider the test case pass. We are using self.totalQueries
        #   to double check that work has actually been done.

        if not mainThread:
            getVMEnv().attachCurrentThread()
        time.sleep(0.5)

        searcher = IndexSearcher(self.directory)
        try:
            self.query = PhraseQuery()
            for word, count in self.testData[0:runCount]:
                query = TermQuery(Term("field", word))
                result = searcher.search(query)
                self.assertEqual(result.length(), count)

                self.lock.acquire()
                self.totalQueries += 1
                self.lock.release()
        finally:
            searcher.close()


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
