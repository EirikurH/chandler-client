# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class PrefixQueryTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """

    def testPrefixQuery(self):

        directory = RAMDirectory()

        categories = ["/Computers", "/Computers/Mac", "/Computers/Windows"]
        writer = IndexWriter(directory, WhitespaceAnalyzer(), True)
        for category in categories:
            doc = Document()
            doc.add(Field("category", category,
                          Field.Store.YES, Field.Index.UN_TOKENIZED))
            writer.addDocument(doc)

        writer.close()

        query = PrefixQuery(Term("category", "/Computers"))
        searcher = IndexSearcher(directory)
        hits = searcher.search(query)
        self.assertEqual(3, hits.length(),
                         "All documents in /Computers category and below")

        query = PrefixQuery(Term("category", "/Computers/Mac"))
        hits = searcher.search(query)
        self.assertEqual(1, hits.length(), "One in /Computers/Mac")


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
