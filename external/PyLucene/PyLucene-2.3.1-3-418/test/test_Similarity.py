# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class SimpleSimilarity(PythonSimilarity):

    def lengthNorm(self, field, numTerms):
        return 1.0

    def queryNorm(self, sumOfSquaredWeights):
        return 1.0

    def tf(self, freq):
        return freq

    def sloppyFreq(self, distance):
        return 2.0

    def idfTerms(self, terms, searcher):
        return 1.0

    def idfTerm(self, term, searcher):
        return 1.0

    def idf(self, docFreq, numDocs):
        return 1.0

    def coord(self, overlap, maxOverlap):
        return 1.0


class SimilarityTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """

    def testSimilarity(self):

        store = RAMDirectory()
        writer = IndexWriter(store, SimpleAnalyzer(), True)
        writer.setSimilarity(SimpleSimilarity())
    
        d1 = Document()
        d1.add(Field("field", "a c",
                     Field.Store.YES, Field.Index.TOKENIZED))

        d2 = Document()
        d2.add(Field("field", "a b c",
                     Field.Store.YES, Field.Index.TOKENIZED))
    
        writer.addDocument(d1)
        writer.addDocument(d2)
        writer.optimize()
        writer.close()

        searcher = IndexSearcher(store)
        searcher.setSimilarity(SimpleSimilarity())

        a = Term("field", "a")
        b = Term("field", "b")
        c = Term("field", "c")

        class hitCollector1(PythonHitCollector):
            def __init__(_self, score):
                super(hitCollector1, _self).__init__()
                _self.score = score
            def collect(_self, doc, score):
                self.assertEqual(score, _self.score)

        searcher.search(TermQuery(b), hitCollector1(1.0))

        bq = BooleanQuery()
        bq.add(TermQuery(a), BooleanClause.Occur.SHOULD)
        bq.add(TermQuery(b), BooleanClause.Occur.SHOULD)

        class hitCollector2(PythonHitCollector):
            def collect(_self, doc, score):
                self.assertEqual(score, doc + 1)

        searcher.search(bq, hitCollector2())

        pq = PhraseQuery()
        pq.add(a)
        pq.add(c)

        searcher.search(pq, hitCollector1(1.0))

        pq.setSlop(2)
        searcher.search(pq, hitCollector1(2.0))


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
