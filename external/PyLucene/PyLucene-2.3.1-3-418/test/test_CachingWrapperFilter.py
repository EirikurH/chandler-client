# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class CachingWrapperFilterTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """

    def testCachingWorks(self):

        dir = RAMDirectory()
        writer = IndexWriter(dir, StandardAnalyzer(), True)
        writer.close()

        reader = IndexReader.open(dir)

        class mockFilter(PythonFilter):
            def __init__(self):
                super(mockFilter, self).__init__()
                self._wasCalled = False
            def bits(self, reader):
                self._wasCalled = True;
                return BitSet()
            def clear(self):
                self._wasCalled = False
            def wasCalled(self):
                return self._wasCalled

        filter = mockFilter()
        cacher = CachingWrapperFilter(filter)

        # first time, nested filter is called
        cacher.bits(reader)
        self.assert_(filter.wasCalled(), "first time")

        # second time, nested filter should not be called
        filter.clear()
        cacher.bits(reader)
        self.assert_(not filter.wasCalled(), "second time")

        reader.close()


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
