# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

import unittest
from lucene import *

class Test_Bug1763(unittest.TestCase):

    def setUp(self):

        self.analyzer = StandardAnalyzer()
        self.d1 = RAMDirectory()
        self.d2 = RAMDirectory()
        
        w1, w2 = [IndexWriter(d, self.analyzer, True)
                  for d in [self.d1, self.d2]]
        doc1 = Document()
        doc2 = Document()
        doc1.add(Field("all", "blah blah double blah Gesundheit",
                       Field.Store.NO, Field.Index.TOKENIZED))
        doc1.add(Field('id', '1', Field.Store.YES, Field.Index.NO))
        doc2.add(Field("all", "a quick brown test ran over the lazy data",
                       Field.Store.NO, Field.Index.TOKENIZED))
        doc2.add(Field('id', '2',
                       Field.Store.YES, Field.Index.NO))
        w1.addDocument(doc1)
        w2.addDocument(doc2)
        for w in [w1, w2]:
            w.optimize()
            w.close()

    def tearDown(self):
        pass

    def test_bug1763(self):
            
        w1 = IndexWriter(self.d1, self.analyzer, True)
        w1.addIndexes([self.d2])
        w1.optimize()
        w1.close()

        searcher = IndexSearcher(self.d1)
        q = QueryParser('all', self.analyzer).parse('brown')
        hits = searcher.search(q)
        self.assertEqual(hits.doc(0).get('id'), '2')


if __name__ == '__main__':
    import lucene
    lucene.initVM(lucene.CLASSPATH)
    unittest.main()
