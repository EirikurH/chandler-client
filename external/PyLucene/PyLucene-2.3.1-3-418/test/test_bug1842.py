# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

import unittest
from lucene import *

class Test_Bug1842(unittest.TestCase):

    def setUp(self):

        self.analyzer = StandardAnalyzer()
        self.d1 = RAMDirectory()
        
        w1 = IndexWriter(self.d1, self.analyzer, True)
        doc1 = Document()
        doc1.add(Field("all", "blah blah blah Gesundheit",
                       Field.Store.NO, Field.Index.TOKENIZED,
                       Field.TermVector.YES))
        doc1.add(Field('id', '1',
                       Field.Store.YES, Field.Index.UN_TOKENIZED))
        w1.addDocument(doc1)
        w1.optimize()
        w1.close()

    def tearDown(self):
        pass

    def test_bug1842(self):
        reader = IndexReader.open(self.d1)
        searcher = IndexSearcher(self.d1)
        q = TermQuery(Term("id", '1'))
        hits = searcher.search(q)
        freqvec = reader.getTermFreqVector(hits.id(0), "all")
        terms = freqvec.getTerms()
        terms.sort()
        self.assert_(terms == ['blah', 'gesundheit'])

        freqs = freqvec.getTermFrequencies()
        self.assert_(freqs == [3, 1])

if __name__ == '__main__':
    import lucene
    lucene.initVM(lucene.CLASSPATH)
    unittest.main()
