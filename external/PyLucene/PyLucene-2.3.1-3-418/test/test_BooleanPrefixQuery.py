# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class BooleanPrefixQueryTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """

    def testMethod(self):

        directory = RAMDirectory()
        categories = ["food", "foodanddrink",
                      "foodanddrinkandgoodtimes", "food and drink"]

        rw1 = None
        rw2 = None

#        try:
        writer = IndexWriter(directory, WhitespaceAnalyzer(), True)
        for category in categories:
            doc = Document()
            doc.add(Field("category", category,
                          Field.Store.YES, Field.Index.UN_TOKENIZED))
            writer.addDocument(doc)

        writer.close()
      
        reader = IndexReader.open(directory)
        query = PrefixQuery(Term("category", "foo"))
      
        rw1 = query.rewrite(reader)
        bq = BooleanQuery()
        bq.add(query, BooleanClause.Occur.MUST)
      
        rw2 = bq.rewrite(reader)
#        except Exception, e:
#            self.fail(str(e))

        bq1 = None
        if BooleanQuery.instance_(rw1):
            bq1 = BooleanQuery.cast_(rw1)
        else:
            self.fail('rewrite')

        bq2 = None
        if BooleanQuery.instance_(rw2):
            bq2 = BooleanQuery.cast_(rw2)
        else:
            self.fail('rewrite')

        self.assertEqual(len(bq1.getClauses()), len(bq2.getClauses()),
                         "Number of Clauses Mismatch")


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
