# -*- coding: utf-8 -*-
# ====================================================================
# Copyright (c) 2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import ThaiAnalyzer, StringReader


class ThaiAnalyzerTestCase(TestCase):

    def assertAnalyzesTo(self, analyzer, input, output):

        tokenStream = analyzer.tokenStream("dummy", StringReader(input))

        for termText in output:
            token = tokenStream.next()
            self.assert_(token is not None)
            self.assertEqual(token.termText(), termText)

        self.assert_(not list(tokenStream))
        tokenStream.close()

    def testAnalyzer(self):

        analyzer = ThaiAnalyzer()
    
        self.assertAnalyzesTo(analyzer, u"", [])

        self.assertAnalyzesTo(analyzer,
                              u"การที่ได้ต้องแสดงว่างานดี",
                              [ u"การ", u"ที่", u"ได้", u"ต้อง",
                                u"แสดง", u"ว่า", u"งาน", u"ดี" ])

        self.assertAnalyzesTo(analyzer,
                              u"บริษัทชื่อ XY&Z - คุยกับ xyz@demo.com",
                              [ u"บริษัท", u"ชื่อ", u"xy&z", u"คุย", u"กับ", u"xyz@demo.com" ])

        # English stop words
        self.assertAnalyzesTo(analyzer,
                              u"ประโยคว่า The quick brown fox jumped over the lazy dogs",
                              [ u"ประโยค", u"ว่า", u"quick", u"brown", u"fox",
                                u"jumped", u"over", u"lazy", u"dogs" ])


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
        main()
