# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import Document, Field, StandardAnalyzer, IndexModifier

MinSizeForCompress = 50

class CompressTestCase(TestCase):

    def addField(self, doc, name, value):

        if len(value) > MinSizeForCompress:
            storeFlag = Field.Store.COMPRESS
        else:
            storeFlag = Field.Store.YES

        doc.add(Field(name, value, storeFlag, Field.Index.TOKENIZED))

    def indexData(self, idx, data):

        doc = Document()
        for key, val in data.iteritems():
            self.addField(doc, key, val)
            idx.addDocument(doc)

    def writeData(self, indexdir, data):

        idx = IndexModifier(indexdir, StandardAnalyzer(), True)
        idx.setUseCompoundFile(True)
        self.indexData(idx, data)
        idx.close()

    def testCompress(self):

        indexdir = 't'
        data = {'uri': "/testing/dict/index/",
                'title': "dict index example",
                'contents': "This index uses PyLucene, and writes dict data in the index."}

        self.writeData(indexdir, data)


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
        main()
