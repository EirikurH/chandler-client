# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class FilteredQueryTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """

    def setUp(self):

        self.directory = RAMDirectory()
        writer = IndexWriter(self.directory, WhitespaceAnalyzer(), True)

        doc = Document()
        doc.add(Field("field", "one two three four five",
                      Field.Store.YES, Field.Index.TOKENIZED))
        doc.add(Field("sorter", "b",
                      Field.Store.YES, Field.Index.TOKENIZED))
                      
        writer.addDocument(doc)

        doc = Document()
        doc.add(Field("field", "one two three four",
                      Field.Store.YES, Field.Index.TOKENIZED))
        doc.add(Field("sorter", "d",
                      Field.Store.YES, Field.Index.TOKENIZED))

        writer.addDocument(doc)

        doc = Document()
        doc.add(Field("field", "one two three y",
                      Field.Store.YES, Field.Index.TOKENIZED))
        doc.add(Field("sorter", "a",
                      Field.Store.YES, Field.Index.TOKENIZED))

        writer.addDocument(doc)

        doc = Document()
        doc.add(Field("field", "one two x",
                      Field.Store.YES, Field.Index.TOKENIZED))
        doc.add(Field("sorter", "c",
                      Field.Store.YES, Field.Index.TOKENIZED))
                      
        writer.addDocument(doc)

        writer.optimize()
        writer.close()

        self.searcher = IndexSearcher(self.directory)
        self.query = TermQuery(Term("field", "three"))

        class filter(PythonFilter):
            def bits(self, reader):
                bitset = BitSet(5)
                bitset.set(1)
                bitset.set(3)
                return bitset

        self.filter = filter()

    def tearDown(self):

        self.searcher.close()
        self.directory.close()

    def testFilteredQuery(self):

        filteredquery = FilteredQuery(self.query, self.filter)
        hits = self.searcher.search(filteredquery);
        self.assertEqual(1, hits.length())
        self.assertEqual(1, hits.id(0))

        hits = self.searcher.search(filteredquery, Sort("sorter"))
        self.assertEqual(1, hits.length())
        self.assertEqual(1, hits.id(0))

        filteredquery = FilteredQuery(TermQuery(Term("field", "one")),
                                      self.filter)
        hits = self.searcher.search(filteredquery)
        self.assertEqual(2, hits.length())

        filteredquery = FilteredQuery(TermQuery(Term("field", "x")),
                                      self.filter)
        hits = self.searcher.search(filteredquery)
        self.assertEqual(1, hits.length())
        self.assertEqual(3, hits.id(0))

        filteredquery = FilteredQuery(TermQuery(Term("field", "y")),
                                      self.filter)
        hits = self.searcher.search(filteredquery)
        self.assertEqual(0, hits.length())

    def testRangeQuery(self):
        """
        This tests FilteredQuery's rewrite correctness
        """
        
        rq = RangeQuery(Term("sorter", "b"), Term("sorter", "d"), True)
        filteredquery = FilteredQuery(rq, self.filter)
        hits = self.searcher.search(filteredquery)
        self.assertEqual(2, hits.length())


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
