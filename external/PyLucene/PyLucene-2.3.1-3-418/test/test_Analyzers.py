# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class AnalyzersTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """

    def _assertAnalyzesTo(self, a, input, output):

        ts = a.tokenStream("dummy", StringReader(input))
        for string in output:
            t = ts.next()
            self.assert_(t is not None)
            self.assertEqual(t.termText(), string)

        self.assert_(not list(ts))
        ts.close()


    def testSimple(self):

        a = SimpleAnalyzer()
        self._assertAnalyzesTo(a, "foo bar FOO BAR", 
                               [ "foo", "bar", "foo", "bar" ])
        self._assertAnalyzesTo(a, "foo      bar .  FOO <> BAR", 
                               [ "foo", "bar", "foo", "bar" ])
        self._assertAnalyzesTo(a, "foo.bar.FOO.BAR", 
                               [ "foo", "bar", "foo", "bar" ])
        self._assertAnalyzesTo(a, "U.S.A.", 
                               [ "u", "s", "a" ])
        self._assertAnalyzesTo(a, "C++", 
                               [ "c" ])
        self._assertAnalyzesTo(a, "B2B", 
                               [ "b", "b" ])
        self._assertAnalyzesTo(a, "2B", 
                               [ "b" ])
        self._assertAnalyzesTo(a, "\"QUOTED\" word", 
                               [ "quoted", "word" ])

    def testNull(self):

        a = WhitespaceAnalyzer()
        self._assertAnalyzesTo(a, "foo bar FOO BAR", 
                               [ "foo", "bar", "FOO", "BAR" ])
        self._assertAnalyzesTo(a, "foo      bar .  FOO <> BAR", 
                               [ "foo", "bar", ".", "FOO", "<>", "BAR" ])
        self._assertAnalyzesTo(a, "foo.bar.FOO.BAR", 
                               [ "foo.bar.FOO.BAR" ])
        self._assertAnalyzesTo(a, "U.S.A.", 
                               [ "U.S.A." ])
        self._assertAnalyzesTo(a, "C++", 
                               [ "C++" ])
        self._assertAnalyzesTo(a, "B2B", 
                               [ "B2B" ])
        self._assertAnalyzesTo(a, "2B", 
                               [ "2B" ])
        self._assertAnalyzesTo(a, "\"QUOTED\" word", 
                               [ "\"QUOTED\"", "word" ])

    def testStop(self):

        a = StopAnalyzer()
        self._assertAnalyzesTo(a, "foo bar FOO BAR", 
                               [ "foo", "bar", "foo", "bar" ])
        self._assertAnalyzesTo(a, "foo a bar such FOO THESE BAR", 
                               [ "foo", "bar", "foo", "bar" ])


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
