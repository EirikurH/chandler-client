# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class TestRegexQuery(TestCase):

    FN = "field"

    def setUp(self):

        directory = RAMDirectory()

        writer = IndexWriter(directory, SimpleAnalyzer(), True)
        doc = Document()
        doc.add(Field(self.FN, "the quick brown fox jumps over the lazy dog", Field.Store.NO, Field.Index.TOKENIZED))
        writer.addDocument(doc)
        writer.optimize()
        writer.close()
        self.searcher = IndexSearcher(directory)

    def tearDown(self):

        self.searcher.close()

    def newTerm(self, value):
  
        return Term(self.FN, value)

    def regexQueryNrHits(self, regex):

        query = RegexQuery(self.newTerm(regex))

        return len(self.searcher.search(query))

    def spanRegexQueryNrHits(self, regex1, regex2, slop, ordered):

        srq1 = SpanRegexQuery(self.newTerm(regex1))
        srq2 = SpanRegexQuery(self.newTerm(regex2))
        query = SpanNearQuery([srq1, srq2], slop, ordered)

        return len(self.searcher.search(query))

    def testRegex1(self):

        self.assertEqual(1, self.regexQueryNrHits("^q.[aeiou]c.*$"))

    def testRegex2(self):

        self.assertEqual(0, self.regexQueryNrHits("^.[aeiou]c.*$"))

    def testRegex3(self):

        self.assertEqual(0, self.regexQueryNrHits("^q.[aeiou]c$"))

    def testSpanRegex1(self):

        self.assertEqual(1, self.spanRegexQueryNrHits("^q.[aeiou]c.*$",
                                                      "dog", 6, True))

    def testSpanRegex2(self):

        self.assertEqual(0, self.spanRegexQueryNrHits("^q.[aeiou]c.*$",
                                                      "dog", 5, True))


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
        main()
