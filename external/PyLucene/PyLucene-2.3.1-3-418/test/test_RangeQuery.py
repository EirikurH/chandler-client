# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class PrefixQueryTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """

    def _initializeIndex(self, values):

        writer = IndexWriter(self.dir, WhitespaceAnalyzer(), True)
        for value in values:
            self._insertDoc(writer, value)
        writer.close()

    def _insertDoc(self, writer, content):

        doc = Document()

        doc.add(Field("id", "id" + str(self.docCount),
                      Field.Store.YES, Field.Index.UN_TOKENIZED))
        doc.add(Field("content", content,
                      Field.Store.NO, Field.Index.TOKENIZED))

        writer.addDocument(doc)
        self.docCount += 1

    def _addDoc(self, content):

        writer = IndexWriter(self.dir, WhitespaceAnalyzer(), False)
        self._insertDoc(writer, content)
        writer.close()

    def setUp(self):

        self.docCount = 0
        self.dir = RAMDirectory()

    def testExclusive(self):

        query = RangeQuery(Term("content", "A"),
                           Term("content", "C"),
                           False)
        self._initializeIndex(["A", "B", "C", "D"])
        searcher = IndexSearcher(self.dir)
        hits = searcher.search(query)
        self.assertEqual(1, hits.length(),
                         "A,B,C,D, only B in range")
        searcher.close()

        self._initializeIndex(["A", "B", "D"])
        searcher = IndexSearcher(self.dir)
        hits = searcher.search(query)
        self.assertEqual(1, hits.length(),
                         "A,B,D, only B in range")
        searcher.close()

        self._addDoc("C")
        searcher = IndexSearcher(self.dir)
        hits = searcher.search(query)
        self.assertEqual(1, hits.length(),
                         "C added, still only B in range")
        searcher.close()

    def testInclusive(self):

        query = RangeQuery(Term("content", "A"),
                           Term("content", "C"),
                           True)

        self._initializeIndex(["A", "B", "C", "D"])
        searcher = IndexSearcher(self.dir)
        hits = searcher.search(query)
        self.assertEqual(3, hits.length(),
                         "A,B,C,D - A,B,C in range")
        searcher.close()

        self._initializeIndex(["A", "B", "D"])
        searcher = IndexSearcher(self.dir)
        hits = searcher.search(query)
        self.assertEqual(2, hits.length(),
                         "A,B,D - A and B in range")
        searcher.close()

        self._addDoc("C")
        searcher = IndexSearcher(self.dir)
        hits = searcher.search(query)
        self.assertEqual(3, hits.length(),
                         "C added - A, B, C in range")
        searcher.close()


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
