# ====================================================================
# Copyright (c) 2004-2007 Open Source Applications Foundation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions: 
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software. 
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# ====================================================================
#

from unittest import TestCase, main
from lucene import *


class DocBoostTestCase(TestCase):
    """
    Unit tests ported from Java Lucene
    """
  
    def testDocBoost(self):

        store = RAMDirectory()
        writer = IndexWriter(store, SimpleAnalyzer(), True)
    
        f1 = Field("field", "word", Field.Store.YES, Field.Index.TOKENIZED)
        f2 = Field("field", "word", Field.Store.YES, Field.Index.TOKENIZED)
        f2.setBoost(2.0)
    
        d1 = Document()
        d2 = Document()
        d3 = Document()
        d4 = Document()
        d3.setBoost(3.0)
        d4.setBoost(2.0)
    
        d1.add(f1)                                 # boost = 1
        d2.add(f2)                                 # boost = 2
        d3.add(f1)                                 # boost = 3
        d4.add(f2)                                 # boost = 4
    
        writer.addDocument(d1)
        writer.addDocument(d2)
        writer.addDocument(d3)
        writer.addDocument(d4)
        writer.optimize()
        writer.close()

        scores = [0.0] * 4

        class hitCollector(PythonHitCollector):
            def collect(self, doc, score):
                scores[doc] = score

        IndexSearcher(store).search(TermQuery(Term("field", "word")),
                                    hitCollector())
    
        lastScore = 0.0
        for score in scores:
            self.assert_(score > lastScore)
            lastScore = score


if __name__ == "__main__":
    import sys, lucene
    lucene.initVM(lucene.CLASSPATH)
    if '-loop' in sys.argv:
        sys.argv.remove('-loop')
        while True:
            try:
                main()
            except:
                pass
    else:
         main()
