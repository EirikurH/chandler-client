
JCC is a C++ code generator for producing the glue code necessary to call
into Java classes from CPython via Java's Native Invocation Interface (JNI).

JCC generates C++ wrapper classes that hide all the gory details of JNI
access as well Java memory and object reference management.

JCC generates CPython types that make these C++ classes accessible from a
Python interpreter. JCC attempts to make these Python types pythonic by
detecting iterators and property accessors. Iterators and mappings may also
be declared to JCC.

JCC has been built on Python 2.3, 2.4 and 2.5 and has been used with various
Java Runtime Environments such as Sun Java 1.4, 1.5 and 1.6, Apple's Java
1.4 and 1.5 on Mac OS X and open source Java OpenJDK 1.7 builds.

JCC is known to work on Intel and PowerPC Mac OS X 10.3, 10.4 and 10.5,
Ubuntu Linux Dapper, Feisty and Gutsy, Sun Solaris Express, Windows 2000 and
Windows XP.

JCC is written in C++ and Python. It uses Java's reflection API to do its
job and needs a Java Runtime Environment to be present to operate.

JCC is built with distutils::

    python setup.py build
    sudo python setup.py install

The setuptools package is required to build JCC on Python 2.3.

Except for Mac OS X - where Apple's Java comes pre-installed in a known
framework location - JCC's setup.py file needs to be edited before being
built to specify the location of the Java Runtime Environment's header files
and libraries.

The svn sources for JCC are available at:
  http://svn.osafoundation.org/pylucene/trunk/jcc/jcc/

For more information about JCC see:
  http://svn.osafoundation.org/pylucene/trunk/jcc/jcc/README 
  http://svn.osafoundation.org/pylucene/trunk/jcc/jcc/INSTALL

For the changes since earlier releases, see:
  http://svn.osafoundation.org/pylucene/trunk/jcc/jcc/CHANGES
