/* ====================================================================
 * Copyright (c) 2004-2008 Open Source Applications Foundation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions: 
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software. 
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * ====================================================================
 */

package org.osafoundation.util;

import java.util.Set;
import java.util.Collection;
import java.util.Iterator;
import java.lang.reflect.Array;


public class PythonSet implements Set {

    private long pythonObject;

    public PythonSet()
    {
    }

    public void pythonExtension(long pythonObject)
    {
        this.pythonObject = pythonObject;
    }
    public long pythonExtension()
    {
        return this.pythonObject;
    }

    public void finalize()
        throws Throwable
    {
        pythonDecRef();
    }

    public native void pythonDecRef();

    public native boolean add(Object obj);
    public native boolean addAll(Collection c);
    public native void clear();
    public native boolean contains(Object obj);
    public native boolean containsAll(Collection c);
    public native boolean equals(Object obj);
    public native boolean isEmpty();
    public native Iterator iterator();
    public native boolean remove(Object obj);
    public native boolean removeAll(Collection c);
    public native boolean retainAll(Collection c);
    public native int size();
    public native Object[] toArray();
    
    public Object[] toArray(Object[] a)
    {
        Object[] array = toArray();
        
        if (a.length < array.length)
            a = (Object[]) Array.newInstance(a.getClass().getComponentType(),
                                             array.length);

        System.arraycopy(array, 0, a, 0, array.length);

        return a;
    }
}
