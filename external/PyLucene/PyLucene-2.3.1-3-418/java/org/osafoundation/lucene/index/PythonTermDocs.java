/* ====================================================================
 * Copyright (c) 2007-2007 Open Source Applications Foundation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions: 
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software. 
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * ====================================================================
 */

package org.osafoundation.lucene.index;

import java.io.IOException;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.index.TermDocs;


public class PythonTermDocs implements TermDocs {

    protected TermDocs termDocs;

    public PythonTermDocs(TermDocs termDocs)
    {
        this.termDocs = termDocs;
    }

    public void seek(Term term)
        throws IOException
    {
        termDocs.seek(term);
    }

    public void seek(TermEnum termEnum) 
        throws IOException
    {
        termDocs.seek(termEnum);
    }

    public int doc()
    {
        return termDocs.doc();
    }

    public int freq()
    {
        return termDocs.freq();
    }

    public boolean next() 
        throws IOException
    {
        return termDocs.next();
    }

    public int read(int[] docs, int[] freqs)
        throws IOException
    {
        return termDocs.read(docs, freqs);
    }

    public int[] read(int n)
        throws IOException
    {
        int[] docs = new int[n];
        int[] freqs = new int[n];
        int[] results;

        n = read(docs, freqs);
        results = new int[n*2];
        System.arraycopy(docs, 0, results, 0, n);
        System.arraycopy(freqs, 0, results, n, n);

        return results;
    }

    public boolean skipTo(int target) 
        throws IOException
    {
        return termDocs.skipTo(target);
    }

    public void close()
        throws IOException
    {
        termDocs.close();
    }
}
