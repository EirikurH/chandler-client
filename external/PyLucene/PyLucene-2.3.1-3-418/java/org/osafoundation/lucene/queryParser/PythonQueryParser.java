/* ====================================================================
 * Copyright (c) 2004-2008 Open Source Applications Foundation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions: 
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software. 
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * ====================================================================
 */

package org.osafoundation.lucene.queryParser;

import java.util.Vector;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.queryParser.CharStream;
import org.apache.lucene.queryParser.ParseException;


public class PythonQueryParser extends QueryParser {

    private long pythonObject;

    public PythonQueryParser(String field, Analyzer analyzer)
    {
        super(field, analyzer);
    }

    public PythonQueryParser(CharStream stream)
    {
        super(stream);
    }

    public void pythonExtension(long pythonObject)
    {
        this.pythonObject = pythonObject;
    }
    public long pythonExtension()
    {
        return this.pythonObject;
    }

    public void finalize()
        throws Throwable
    {
        pythonDecRef();
    }

    public native void pythonDecRef();
    public native Query getBooleanQuery(Vector clauses);
    public native Query getFieldQuery(String field, String queryText);
    public native Query getFieldQuery(String field, String queryText, int slop);
    public native Query getFuzzyQuery(String field, String termText,
                                      float minSimilarity);
    public native Query getPrefixQuery(String field, String termText);
    public native Query getRangeQuery(String field,
                                      String part1, String part2,
                                      boolean inclusive);
    public native Query getWildcardQuery(String field, String termText);
}
